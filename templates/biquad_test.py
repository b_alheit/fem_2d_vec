try:
    import sys
    sys.path.append(sys.path[0][:sys.path[0].find('fem_2d_vec')] + 'fem_2d_vec/modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements
except:
    print("Modules may be imported incorrectly due to invalid directory structure.\n"
          "Structure should be: ./fem_2d_vec/.../<this script>.py\n"
          "                     ./fem_2d_vec/modules")

import numpy as np
import math


def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return 0.3
    # return 0.49995

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 250


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs(x - 48)/48 < 0.000000000001:
        return np.array([0, 100/16])
    else:
        return np.array([0, 0])



def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])

nodes_x = np.array([0, 48, 48, 0, 24, 48, 24, 0, 24])
nodes_y = np.array([0, 44, 60, 44, 22, 52, 52, 22, 37])
BC = np.zeros(18)
BC[[0, 1, 6, 7, 14, 15]] = 1
ICA = np.array([[1, 2, 3, 4, 5, 6, 7, 8, 9]])

# n = 25
# x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
# y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes
# # nodes_x = np.insert(np.linspace(0, 1, n), np.arange(0, n), np.linspace(0, 1, n))    # The x positions of the nodes
# # nodes_y = np.array([0, 0.1] * n)    # The y positions of the nodes
# nodes = (n+1) ** 2
# els = (n/2) ** 2
# x_12 = x_verticies[1] - x_verticies[0]
# y_12 = y_verticies[1] - y_verticies[0]
# y_23 = y_verticies[2] - y_verticies[1]
# y_14 = y_verticies[3] - y_verticies[0]
#
# nodes_x = np.zeros(nodes)
# nodes_y = np.zeros(nodes)
#
# BC = np.zeros(nodes*2)
# # BC[[0, 1, 6, 7, 14, 15]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
# c = 0
#
# for i in range(n + 1):
#     y_start = i * y_14/n
#     y_end = y_verticies[1] + i * y_23/n
#     y_diff = y_end - y_start
#     for j in range(n + 1):
#         nodes_x[c] = j*x_12/n
#         nodes_y[c] = y_start + j * y_diff / n
#         if nodes_x[c] == 0:
#             BC[[2*c, 2*c+1]] = 1
#         c += 1
#
#
# ICA = np.zeros([els, 9], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)
#
# for i in range(els):
#     bl = i + 1 + math.floor(i/n)
#     # ICA[i, :] = np.array([bl, bl+1, bl + n + 2, bl + n + 1])
#     ICA[i, :] = [int(bl), int(bl+1), int(bl + n + 2), int(bl + n + 1)]

# Creating a triangle mesh object with given set up
quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x, nodes_y, ICA, E, v, t_bar, u_bar, b, BC, "strain", thick, continuous_2D_vector_elements.Q2)
# quad_mesh.display_mesh("blahh")

# tri_mesh.display_mesh("big mesh", False, False, False)
quad_mesh.int_F_bound(5)
quad_mesh.int_K(5)   # Performing integration to create stiffness matricies
quad_mesh.int_F_bod(5)   # Performing integration to create body force vectors
quad_mesh.assemble()     # Assembling element matrices and vectors into global
quad_mesh.solve_d()      # Solving for global d

print("Max deflection: ", np.max(quad_mesh.d))
# print("Deflection : ", quad_mesh.d)    # Displaying node deflections
# print("Original node positions: ", quad_mesh.pos)    # Displaying old node positions
# print("New node positions: ", quad_mesh.pos + quad_mesh.d)    # Displaying new node positions

# for i in range(len(quad_mesh.Elements)):
#     e = quad_mesh.Elements[i]
#     print("Stress in el "+str(i+1)+": ", np.matmul(quad_mesh.E_mat(0, 0), np.matmul(e.B(0, 0), e.d))) # Calculating and displaying stress in element 2

quad_mesh.plot_displacement("Plane strain beam solution", 1, 12, False, True, False)   # Plotting the old and new node positions
