import sys
try:
    sys.path.append('../modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements as ct
    import discontinuous_2D_scalar_elements as dt
except:
    try:
        sys.path.append('./modules')
        import Mesh_2D_Vec
        import continuous_2D_vector_elements as ct
        import discontinuous_2D_scalar_elements as dt

    except:
        print("Incorrect project structure: Can't find modules")


import numpy as np
import matplotlib.pyplot as plt
import math


"""
****** change vtest to change Poison's ratio ********
"""


L = 10
l = 2
f_max = 3000
vtest = 0.499
Ein = 1500

scale = 0.01

points = 100

xh = np.linspace(0, L, points)
xv = np.zeros(points)
yh = np.zeros(points)
yv = np.linspace(0, l, points)

x = np.append(xh, xv + L)
x = np.append(x, np.flip(xh, axis=0))
x = np.append(x, xv)

y = np.append(yh, yv)
y = np.append(y, yh + l)
y = np.append(y, np.flip(yh, axis=0))

def get_ux(x, y, vtest):
    return (2 * f_max * (1-(vtest**2)) / (Ein * l)) * np.multiply(x, (l/2 - y))

def get_uy(x, y, vtest):
    return (f_max * (1-(vtest**2)) / (Ein * l)) * (np.power(x, 2) + vtest/(1-vtest) * np.multiply(y, y-l))



def p1(vin):

    def thick(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The thickness at position (x,y)
        """
        return 1


    def v(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Poison's ratio at position (x,y)
        """
        return vin

    def E(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Young's modulus at position (x,y)
        """
        return Ein


    def b(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The body force at position (x,y)
        """
        # return np.array([0, - 2e5 * thick(x, y)])
        return np.array([0, 0])


    def t_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed traction at position (x,y)
        """
        # return np.array([0, -50])
        if abs(x - L) < 0.000000000001:
            return np.array([(2*f_max/l)*(l/2 - y), 0])
        else:
            return np.array([0, 0])



    def u_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed displacement at position (x,y)
        """
        if x == 0:
            return np.array([0, 0])
        else:
            return np.array([None, None])

    # n = 4
    # row_nodes = n + 1
    # x_verticies = np.array([0, L, L, 0])  # The x positions of the nodes
    # y_verticies = np.array([0, 0, l, l])  # The y positions of the nodes
    # # nodes_x = np.insert(np.linspace(0, 1, n), np.arange(0, n), np.linspace(0, 1, n))    # The x positions of the nodes
    # # nodes_y = np.array([0, 0.1] * n)    # The y positions of the nodes
    # nodes = (n + 1) ** 2
    # els = 2 * (n ** 2)
    # x_12 = x_verticies[1] - x_verticies[0]
    # y_12 = y_verticies[1] - y_verticies[0]
    # y_23 = y_verticies[2] - y_verticies[1]
    # y_14 = y_verticies[3] - y_verticies[0]
    #
    # nodes_x = np.zeros(nodes)
    # nodes_y = np.zeros(nodes)
    # # nodes_x = np.array([0, 48, 48, 0])
    # # nodes_y = np.array([0, 44, 60, 44])
    #
    # BC = np.zeros(nodes * 2)
    # # BC[[0, 1, 6, 7]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    # c = 0
    #
    # for i in range(n + 1):
    #     y_start = i * y_14 / n
    #     y_end = y_verticies[1] + i * y_23 / n
    #     y_diff = y_end - y_start
    #     for j in range(n + 1):
    #         nodes_x[c] = j * x_12 / n
    #         nodes_y[c] = y_start + j * y_diff / n
    #         if nodes_x[c] == 0:
    #             BC[[2 * c, 2 * c + 1]] = 1
    #         c += 1
    #
    # ICA = np.zeros([els, 3],
    #                dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)
    #
    # for i in range(int(els / 2)):
    #     p = i % n
    #     p1 = math.floor((i + 1) / row_nodes)
    #     bl = i % n + 1 + math.floor(i / n) * row_nodes
    #     # ICA[i, :] = np.array([bl, bl+1, bl + n + 2, bl + n + 1])
    #     ICA[i * 2, :] = [int(bl), int(bl + 1), int(bl + row_nodes)]
    #     ICA[i * 2 + 1, :] = [int(bl + 1), int(bl + 1 + row_nodes), int(bl + row_nodes)]

    nodes_x = np.array([0, 0, 0, L/4, L/4, L/4, L/2, L/2, L/2, 3*L/4, 3*L/4, 3*L/4, L, L, L])
    nodes_y = np.array([0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l])

    BC = np.zeros(2*np.alen(nodes_x))
    BC[[0, 1, 2, 4]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes



    ICA = np.zeros([16, 3], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    ICA = np.array([[1, 4, 5],
                    [1, 5, 2],
                    [2, 5, 6],
                    [2, 6, 3],
                    [4, 7, 8],
                    [4, 8, 5],
                    [5, 8, 9],
                    [5, 9, 6],
                    [7, 10, 11],
                    [7, 11, 8],
                    [8, 11, 12],
                    [8, 12, 9],
                    [10, 13, 14],
                    [10, 14, 11],
                    [11, 14, 15],
                    [11, 15, 12]])

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                            "strain",
                                            thick,
                                            ct.P1)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K(3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh


p1Mesh = p1(vtest)
# v045 = p1(0.45)
# v0499 = p1(0.499)


ux = get_ux(x, y, vtest)
uy = get_uy(x, y, vtest)

# ux045 = get_ux(x, y, 0.45)
# uy045 = get_uy(x, y, 0.45)
#
# ux0499 = get_ux(x, y, 0.499)
# uy0499 = get_uy(x, y, 0.499)
#

plot = plt

plot.plot(x + scale * ux, y + scale*uy, linestyle="--", linewidth=2, color='grey', label='Analytical solution')
# plot.plot(x + scale * ux045, y + scale*uy045, linestyle=":", linewidth=2, color='grey', label='Analytical solution $ \\nu = 0.45 $')
# plot.plot(x + scale * ux0499, y + scale*uy0499, linestyle="-.", linewidth=2, color='grey', label='Analytical solution $ \\nu = 0.499 $')

plot.grid()
plot.xlabel('x (m)')
plot.ylabel('y (m)')
plot.title('Plot of solutions produced by P1 elements for a Poison\'s ratio of ' + str(vtest) + '\nUsing a scale factor of ' +str(scale) + ' for the displacement')

plot = p1Mesh.add_to_plot(plot,
                          scale=scale,
                          color='black',
                          line_style='-',
                          marker='',
                          legend='P1',
                          lines_per_edge=1,
                          show_nodes=True,
                          show_element_numbers=False,
                          show_nodes_numbers=False)
#
# plot = v045.add_to_plot(plot,
#                           scale=scale,
#                           color='black',
#                           line_style='--',
#                           marker='o',
#                           legend='P1 $\\nu = 0.45 $',
#                           lines_per_edge=1,
#                           show_nodes=True,
#                           show_element_numbers=False,
#                           show_nodes_numbers=False)
#
#
# plot = v0499.add_to_plot(plot,
#                           scale=scale,
#                           color='black',
#                           line_style=':',
#                           marker='v',
#                           legend='P1 $\\nu = 0.499 $',
#                           lines_per_edge=1,
#                           show_nodes=True,
#                           show_element_numbers=False,
#                           show_nodes_numbers=False)

# print("Poisons ratio: ", vtest)
# print("Analytical vertical displacement: ", get_uy(L, l))
print("P1 v=", vtest," vertical displacement: ", p1Mesh.d[-1])
# print("P1 v=0.45 vertical displacement: ", v045.d[-1])
# print("P1 v=0.499 vertical displacement: ", v0499.d[-1])

plot.legend()
plot.show()
