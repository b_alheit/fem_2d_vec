import sys
try:
    sys.path.append('../modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements as ct
    import discontinuous_2D_scalar_elements as dt
except:
    try:
        sys.path.append('./modules')
        import Mesh_2D_Vec
        import continuous_2D_vector_elements as ct
        import discontinuous_2D_scalar_elements as dt

    except:
        print("Incorrect project structure: Can't find modules")


import numpy as np
import matplotlib.pyplot as plt
import math


"""
****** change vtest to change Poison's ratio ********
"""


L = 10
l = 2
f_max = 3000
vtest = 0.499
Ein = 1500
scale = 0.01

points = 100

xh = np.linspace(0, L, points)
xv = np.zeros(points)
yh = np.zeros(points)
yv = np.linspace(0, l, points)

x = np.append(xh, xv + L)
x = np.append(x, np.flip(xh, axis=0))
x = np.append(x, xv)

y = np.append(yh, yv)
y = np.append(y, yh + l)
y = np.append(y, np.flip(yh, axis=0))

def get_ux(x,y):
    return (2 * f_max * (1-(vtest**2)) / (Ein * l)) * np.multiply(x, (l/2 - y))

def get_uy(x, y):
    return (f_max * (1-(vtest**2)) / (Ein * l)) * (np.power(x, 2) + vtest/(1-vtest) * np.multiply(y, y-l))

ux = get_ux(x, y)
uy = get_uy(x, y)


def q1(vin):

    def thick(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The thickness at position (x,y)
        """
        return 1


    def v(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Poison's ratio at position (x,y)
        """
        return vin

    def E(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Young's modulus at position (x,y)
        """
        return 1500


    def b(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The body force at position (x,y)
        """
        # return np.array([0, - 2e5 * thick(x, y)])
        return np.array([0, 0])


    def t_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed traction at position (x,y)
        """
        # return np.array([0, -50])
        if abs(x - L) < 0.000000000001:
            return np.array([(2*f_max/l)*(l/2 - y), 0])
        else:
            return np.array([0, 0])



    def u_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed displacement at position (x,y)
        """
        if x == 0:
            return np.array([0, 0])
        else:
            return np.array([None, None])


    nodes_x = np.array([0, 0, 0, L/4, L/4, L/4, L/2, L/2, L/2, 3*L/4, 3*L/4, 3*L/4, L, L, L])
    nodes_y = np.array([0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l])
    # nodes_x = np.array([0, 48, 48, 0])
    # nodes_y = np.array([0, 44, 60, 44])

    BC = np.zeros(2*np.alen(nodes_x))
    BC[[0, 1, 2, 4]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes



    ICA = np.zeros([8, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    ICA = np.array([[1, 4, 5, 2],
                    [2, 5, 6, 3],
                    [4, 7, 8, 5],
                    [5, 8, 9, 6],
                    [7, 10, 11, 8],
                    [8, 11, 12, 9],
                    [10, 13, 14, 11],
                    [11, 14, 15, 12]])

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                        "strain",
                                            thick,
                                            ct.Q1)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K(3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh




def q1_SRI(vin):

    def thick(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The thickness at position (x,y)
        """
        return 1


    def v(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Poison's ratio at position (x,y)
        """
        return vin

    def E(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Young's modulus at position (x,y)
        """
        return 1500


    def b(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The body force at position (x,y)
        """
        # return np.array([0, - 2e5 * thick(x, y)])
        return np.array([0, 0])


    def t_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed traction at position (x,y)
        """
        # return np.array([0, -50])
        if abs(x - L) < 0.000000000001:
            return np.array([(2*f_max/l)*(l/2 - y), 0])
        else:
            return np.array([0, 0])



    def u_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed displacement at position (x,y)
        """
        if x == 0:
            return np.array([0, 0])
        else:
            return np.array([None, None])


    nodes_x = np.array([0, 0, 0, L/4, L/4, L/4, L/2, L/2, L/2, 3*L/4, 3*L/4, 3*L/4, L, L, L])
    nodes_y = np.array([0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l, 0, l/2, l])
    # nodes_x = np.array([0, 48, 48, 0])
    # nodes_y = np.array([0, 44, 60, 44])

    BC = np.zeros(2*np.alen(nodes_x))
    BC[[0, 1, 2, 4]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes



    ICA = np.zeros([8, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    ICA = np.array([[1, 4, 5, 2],
                    [2, 5, 6, 3],
                    [4, 7, 8, 5],
                    [5, 8, 9, 6],
                    [7, 10, 11, 8],
                    [8, 11, 12, 9],
                    [10, 13, 14, 11],
                    [11, 14, 15, 12]])

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_SRI(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                            "strain",
                                            thick,
                                            ct.Q1)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K_SRI(1, 3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh



def q2(vin):

    def thick(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The thickness at position (x,y)
        """
        return 1


    def v(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Poison's ratio at position (x,y)
        """
        return vin

    def E(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The Young's modulus at position (x,y)
        """
        return 1500


    def b(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The body force at position (x,y)
        """
        # return np.array([0, - 2e5 * thick(x, y)])
        return np.array([0, 0])


    def t_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed traction at position (x,y)
        """
        # return np.array([0, -50])
        if abs(x - L) < 0.000000000001:
            return np.array([(2*f_max/l)*(l/2 - y), 0])
        else:
            return np.array([0, 0])



    def u_bar(x, y):
        """
        :param x: x position
        :param y: y position
        :return: The prescribed displacement at position (x,y)
        """
        if x == 0:
            return np.array([0, 0])
        else:
            return np.array([None, None])


    nodes_x = np.array([0, 0, 0, 0, 0,
                        L/8, L/8, L/8, L/8, L/8,
                        L/4, L/4, L/4, L/4, L/4,
                        3 * L / 8, 3 * L / 8, 3 * L / 8, 3 * L / 8, 3 * L / 8,
                        L/2, L/2, L/2, L/2, L/2,
                        5 * L / 8, 5 * L / 8, 5 * L / 8, 5 * L / 8, 5 * L / 8,
                        3*L/4, 3*L/4, 3*L/4, 3*L/4, 3*L/4,
                        7 * L / 8, 7 * L / 8, 7 * L / 8, 7 * L / 8, 7 * L / 8,
                        L, L, L, L, L])

    nodes_y = np.array([0, l/4, l/2, 3*l/4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l,
                        0, l / 4, l / 2, 3 * l / 4, l
                        ])
    # nodes_x = np.array([0, 48, 48, 0])
    # nodes_y = np.array([0, 44, 60, 44])

    BC = np.zeros(2*np.alen(nodes_x))
    BC[[0, 1, 2, 4, 6, 8]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes



    ICA = np.zeros([8, 9], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)


    ICA = np.array([[1 ,11, 13, 3, 6, 12, 8, 2, 7],
                    [3, 13, 15, 5, 8, 14, 10, 4, 9],
                    [11, 21, 23, 13, 16, 22, 18, 12, 17],
                    [13, 23, 25, 15, 18, 24, 20, 14, 19],
                    [21, 31, 33, 23, 26, 32, 28, 22, 27],
                    [23, 33, 35, 25, 28, 34, 30, 24, 29],
                    [31, 41, 43, 33, 36, 42, 38, 32, 37],
                    [33, 43, 45, 35, 38, 44, 40, 34, 39]])

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                            "strain",
                                            thick,
                                            ct.Q2)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K(3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh


q1test = q1(vtest)
q2test = q2(vtest)
q1SRItest = q1_SRI(vtest)

plot = plt

plot.plot(x + scale * ux, y + scale*uy, linestyle="--", linewidth=4, color='grey', label='Analytical solution')

plot.grid()
plot.xlabel('x (m)')
plot.ylabel('y (m)')
plot.title('Plot of solutions produced by different elements for a Poison\'s ratio of ' + str(vtest) + '\nUsing a scale factor of ' +str(scale) + ' for the displacement')

plot = q1test.add_to_plot(plot,
                          scale=scale,
                          color='black',
                          line_style='-',
                          marker='',
                          legend='Q1',
                          lines_per_edge=1,
                          show_nodes=True,
                          show_element_numbers=False,
                          show_nodes_numbers=False)

plot = q1SRItest.add_to_plot(plot,
                          scale=scale,
                          color='black',
                          line_style='--',
                          marker='',
                          legend='Q1 SRI',
                          lines_per_edge=1,
                          show_nodes=True,
                          show_element_numbers=False,
                          show_nodes_numbers=False)


plot = q2test.add_to_plot(plot,
                          scale=scale,
                          color='black',
                          line_style=':',
                          marker='',
                          legend='Q2',
                          lines_per_edge=4,
                          show_nodes=True,
                          show_element_numbers=False,
                          show_nodes_numbers=False)
print("Poisons ratio: ", vtest)
print("Analytical vertical displacement: ", get_uy(L, l))
print("Q2 vertical displacement: ", q2test.d[-1])
print("Q1 vertical displacement: ", q1test.d[-1])
print("Q1SRI vertical displacement: ", q1SRItest.d[-1])

plot.legend()
plot.show()
