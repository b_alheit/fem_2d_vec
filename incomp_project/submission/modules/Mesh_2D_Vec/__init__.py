import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.lines import Line2D
import numpy as np
import math
import scipy.sparse as sp
import scipy.sparse.linalg as splg
import mixed_elements as me
import continuous_2D_vector_elements as ct
import discontinuous_2D_scalar_elements as dt

import matplotlib.lines as mlines

class Mesh_2D_Vec_Std:
    def __init__(self, x, y, ICA, E, v, t_bar, u_bar, b, BCs, E_type, thick, element):
        self.x = x
        self.y = y
        self.pos = np.insert(y, np.arange(0, len(y)), x)
        self.BCs = BCs
        self.n_nodes = len(x)
        self.DOF = self.n_nodes * 2
        self.ICA = ICA - 1
        self.n_el = len(ICA)
        self.K = np.zeros([self.DOF, self.DOF])
        self.d = np.zeros(self.DOF)
        self.F = np.zeros(self.DOF)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        self.t_bar = t_bar
        self.u_bar = u_bar
        self.b = b
        self.E = E
        self.v = v
        self.E_type = E_type
        self.thick = thick

        self.Elements = np.empty(self.n_el, dtype=element)
        for i in range(self.n_el):
            local_nodes = self.ICA[i]
            self.Elements[i] = element(x[local_nodes], y[local_nodes], local_nodes, i)

    def E_mat(self, x, y):
        if self.E_type == "stress":
            return (self.E(x, y)/(1-self.v(x, y)**2)) * np.array([[1, self.v(x, y), 0],
                                                                  [self.v(x, y), 1, 0],
                                                                  [0, 0, (1 - self.v(x, y))/2]])
        elif self.E_type == "strain":
            return (self.E(x, y)/((1-2*self.v(x, y))*(1+self.v(x, y)))) * np.array([[1 - self.v(x, y), self.v(x, y), 0],
                                                                                    [self.v(x, y), 1 - self.v(x, y), 0],
                                                                                    [0, 0, (1 - 2 * self.v(x, y))/2]])
        else:
            print("Invalid stiffness matrix type (E_type) chosen. Must input either \'stress\' or \'strain\'")
            return None


    def D_lambda(self, x, y):
        return ((self.E(x, y) * self.v(x,y)) / ((1 + self.v(x,y))*(1 - 2 * self.v(x,y)))) * np.array([[1, 1, 0],
                                                                                                      [1, 1, 0],
                                                                                                      [0, 0, 0]])

    def D_mu(self, x, y):
        return (self.E(x, y) / (2 * (1 + self.v(x,y)))) * np.array([[2, 0, 0],
                                                                    [0, 2, 0],
                                                                    [0, 0, 1]])


    def display_mesh(self, title, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            if show_element_numbers:
                cent_x = np.sum(el_x) / len(el_x)
                cent_y = np.sum(el_y) / len(el_y)
                mesh_plot.text(cent_x, cent_y, str(i+1), color='black')

            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')

        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], str(i + 1), color='red', fontsize=10)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def lin_quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        return n_gp, xi, weights

    # def line_map_to_physical(self, xi, x1, x2):
    #     return (x2 + x1)/2 + xi*(x2-x1)/2

    def int_K(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.E_mat(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp, integrand)


    def int_K_SRI(self, n_gp_l, n_gp_m):
        for i in range(self.n_el):
            e = self.Elements[i]

            def int_lambda(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_lambda(x, y), e.B(xi, eta)))

            def int_mu(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_mu(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp_l, int_lambda) + e.gauss(n_gp_m, int_mu)


    def int_F_bod(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.N(xi, eta).T, self.b(x, y))

            e.el_F_bod = e.gauss(n_gp, integrand)

    def int_F_bound(self, n_gp):
        n_gp, xi, weights = self.lin_quad_selector(n_gp)
        for i in range(self.n_el):
            e = self.Elements[i]

            for j in range(e.edges):
                end_index = (j+2) % e.edges-1
                x_edge = np.array([e.x[j], e.x[end_index]])
                y_edge = np.array([e.y[j], e.y[end_index]])
                J = np.sqrt((x_edge[-1] - x_edge[0]) ** 2 + (y_edge[-1] - y_edge[0]) ** 2) / 2

                for k in range(n_gp):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    x, y = pos[0], pos[1]
                    e.el_F_bound += weights[k] * J * np.matmul(N.T,  self.t_bar(x, y))

    def assemble(self):
        self.K = np.zeros([self.DOF, self.DOF])
        self.F = np.zeros(self.DOF)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes*2):
                self.K[e.g_DOF[j], e.g_DOF] += e.el_K[j, :]
            self.F_bod[e.g_DOF] += e.el_F_bod
            self.F_bound[e.g_DOF] += e.el_F_bound
        self.F = self.F_bod + self.F_bound

    def solve_d(self):
        F_cond = self.F
        K_cond = self.K
        s=5
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond -= self.u_bar(self.x[node], self.y[node])[i % 2] * K_cond[:, i]
                K_cond[i, :] = 0
                K_cond[:, i] = 0
                K_cond[i, i] = 1
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond[i] = self.u_bar(self.x[node], self.y[node])[i % 2]
        K_cond_sparse = sp.csr_matrix(K_cond)
        self.d = splg.spsolve(K_cond_sparse, F_cond)
        # self.d = np.matmul(np.linalg.inv(K_cond), F_cond)
        for i in range(self.n_el):
            self.Elements[i].d = self.d[self.Elements[i].g_DOF]

    def solution(self, x, y):
        for i in range(self.n_el):
            e = self.Elements[i]
            if e.in_element(x, y):
                return np.matmul(e.Nxy(x, y), e.d)
        # print("Position not in domain")
        return None

    def display_mesh_2(self, title, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt

        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)

                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')

            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        # mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
        #                   Line2D([0], [0], color='blue')],
        #                  ['Original mesh',
        #                   'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        # x_max, x_min = np.max(x_new), np.min(x_new)
        # mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def plot_displacement(self, title, scale, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers = True):
        mesh_plot = plt
        x_new = self.x + scale*self.d[0::2]
        y_new = self.y + scale*self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                x_pn = []
                y_pn = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale*d[0])
                    y_pn.append(pos[1] + scale*d[1])
                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')
                mesh_plot.plot(x_pn, y_pn, color='blue')
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')
            mesh_plot.scatter(x_new, y_new, color='blue')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
                          Line2D([0], [0], color='blue')],
                         ['Original mesh',
                          'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        x_max, x_min = np.max(x_new), np.min(x_new)
        mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def add_to_plot(self,
                    plot,
                    scale,
                    marker,
                    line_style,
                    legend,
                    color,
                    lines_per_edge=1,
                    show_nodes_numbers=True,
                    show_nodes=True,
                    show_element_numbers=True):

        mesh_plot = plot
        x_new = self.x + scale * self.d[0::2]
        y_new = self.y + scale * self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge + 1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):

                x_pn = []
                y_pn = []
                for k in range(lines_per_edge + 1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale * d[0])
                    y_pn.append(pos[1] + scale * d[1])
                mesh_plot.plot(x_pn, y_pn, color=color, linestyle=line_style)
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        if show_nodes:
            mesh_plot.scatter(x_new, y_new, marker=marker, color=color, zorder=5)
            mesh_plot.plot([], [], marker=marker, color=color, label=legend, linestyle=line_style)

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "(" + str(i + 1) + ")", color='red', fontsize=10)

        return mesh_plot

    def plot_solution(self, x_res, y_res, title, type):
        x = np.linspace(np.min(self.x), np.max(self.x), x_res)
        y = np.linspace(np.min(self.y), np.max(self.y), y_res)
        x, y = np.meshgrid(x, y)
        z = np.zeros([y_res, x_res])
        for i in range(x_res):
            for j in range(y_res):
                z[j, i] = self.solution(x[j, i], y[j, i])

        masked_array = np.ma.array(z, mask=np.isnan(z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        if type == "smooth":
            c_plot = mesh_plot.pcolor(x, y, masked_array, cmap=cmap)
        elif type == "cont":
            c_plot = mesh_plot.contourf(x, y, masked_array, 10, cmap=cmap)
        else:
            print("Error. Invalid type chosen")
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / 3
            cent_y = np.sum(el_y) / 3
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()

    def plot(self, X, Y, Z):

        masked_array = np.ma.array(Z, mask=np.isnan(Z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        c_plot = mesh_plot.contourf(X, Y, masked_array, cmap=cmap)
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "["+str(i+1)+"]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title("FEM solution")
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()
    #
    # def plot_solution_element_wise(self, apprx_n_points):
    #     pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
    #     xi_pos = np.linspace(-1, 1, pp_iso)
    #     eta_pos = xi_pos
    #     Xs = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Ys = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Zs = np.zeros([self.n_el, pp_iso-1, pp_iso-1])
    #     for i in range(self.n_el):
    #         X = np.zeros([pp_iso, pp_iso])
    #         Y = np.zeros([pp_iso, pp_iso])
    #         Z = np.zeros([pp_iso-1, pp_iso-1])
    #         e = self.Elements[i]
    #         for j in range(pp_iso-1):
    #             for k in range(pp_iso-1):
    #                 X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
    #                 X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
    #                 X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
    #                 X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])
    #
    #                 Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
    #                 Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
    #                 Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
    #                 Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])
    #
    #                 xi_av = (xi_pos[j] + xi_pos[j+1])/2
    #                 eta_av = (eta_pos[k] + eta_pos[k+1])/2
    #
    #                 Z[j, k] = np.matmul(e.N(xi_av, eta_av), e.d)
    #         Xs[i] = X
    #         Ys[i] = Y
    #         Zs[i] = Z
    #     masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
    #     cmap = cm.jet
    #     cmap.set_bad('white', 1.)
    #
    #     mesh_plot = plt
    #     min_val, max_val = np.min(Zs), np.max(Zs)
    #
    #     for i in range(self.n_el):
    #         mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #         # mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #     mesh_plot.colorbar()
    #     for i in range(len(self.ICA)):
    #         el_nodes = self.ICA[i]
    #         el_x = self.x[el_nodes]
    #         el_y = self.y[el_nodes]
    #         cent_x = np.sum(el_x) / len(el_x)
    #         cent_y = np.sum(el_y) / len(el_y)
    #         mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
    #         mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')
    #
    #     mesh_plot.scatter(self.x, self.y, color='black')
    #
    #     mesh_plot.xlim(np.min(self.x), np.max(self.x))
    #     mesh_plot.ylim(np.min(self.y), np.max(self.y))
    #     mesh_plot.grid()
    #     mesh_plot.title("FEM solution")
    #     mesh_plot.xlabel("x (m)")
    #     mesh_plot.ylabel("y (m)")
    #     mesh_plot.show()
    #

    def plot_solution_element_wise(self, apprx_n_points, title, type):
        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])
        for i in range(self.n_el):
            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            e = self.Elements[i]
            for j in range(pp_iso):
                for k in range(pp_iso):
                    X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = np.matmul(e.N(xi_pos[j], eta_pos[k]), e.d)
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z
        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        min_val, max_val = np.min(Zs), np.max(Zs)

        for i in range(self.n_el):
            if type == "smooth":
                mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
            elif type == "cont":
                mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], np.linspace(min_val, np.around(max_val, -1), 15), vmin=min_val, vmax=max_val, cmap=cmap)
            else:
                print("Error. Invalid type chosen.")
        mesh_plot.colorbar()

        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()



    def plot_AB(self, res, type):
        x = np.linspace(np.min(self.x), 6, res)
        q = np.zeros([2, len(x)])
        T = np.zeros(len(x))
        for i in range(len(x)):
            for j in range(self.n_el):
                e = self.Elements[j]
                if e.in_element(x[i], 4):
                    q[:, i] = -np.matmul(self.D(x[i], 4), np.matmul(e.B(x[i], 4), e.d))
                    T[i] = np.matmul(e.Nxy(x[i], 4), e.d)
                    break
        if type == "heat":
            flux_plot = plt
            flux_plot.plot(x, q[0, :], label=r"$q_x$")
            flux_plot.plot(x, q[1, :], label=r"$q_y$")
            flux_plot.grid()
            flux_plot.xlabel(r"x $(cm)$")
            flux_plot.ylabel(r"Heat flux $(W)$")
            flux_plot.legend(loc='best')
            flux_plot.title(r"Heat flux along line AB")
            flux_plot.show()

        elif type == "temp":
            e = self.Elements[3]
            T_plot = plt
            T_plot.plot(x[0:len(x)-2], T[0:len(x)-2], label=r"$T$")
            T_plot.plot([0,4,6], [T[0],np.matmul(e.Nxy(4, 4), e.d),T[-2]], linewidth=0, marker='s', color='b')
            T_plot.grid()
            T_plot.xlabel(r"x $(cm)$")
            T_plot.ylabel(r"Temperature $(C)$")
            T_plot.title(r"Temperature along line AB")
            T_plot.show()

class Mesh_2D_Vec_SRI:
    def __init__(self, x, y, ICA, E, v, t_bar, u_bar, b, BCs, E_type, thick, element):
        self.x = x
        self.y = y
        self.pos = np.insert(y, np.arange(0, len(y)), x)
        self.BCs = BCs
        self.n_nodes = len(x)
        self.DOF = self.n_nodes * 2
        self.ICA = ICA - 1
        self.n_el = len(ICA)
        self.K = np.zeros([self.DOF, self.DOF])
        self.d = np.zeros(self.DOF)
        self.F = np.zeros(self.DOF)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        self.t_bar = t_bar
        self.u_bar = u_bar
        self.b = b
        self.E = E
        self.v = v
        self.E_type = E_type
        self.thick = thick

        self.Elements = np.empty(self.n_el, dtype=element)
        for i in range(self.n_el):
            local_nodes = self.ICA[i]
            self.Elements[i] = element(x[local_nodes], y[local_nodes], local_nodes, i)

    def E_mat(self, x, y):
        if self.E_type == "stress":
            return (self.E(x, y)/(1-self.v(x, y)**2)) * np.array([[1, self.v(x, y), 0],
                                                                  [self.v(x, y), 1, 0],
                                                                  [0, 0, (1 - self.v(x, y))/2]])
        elif self.E_type == "strain":
            return (self.E(x, y)/((1-2*self.v(x, y))*(1+self.v(x, y)))) * np.array([[1 - self.v(x, y), self.v(x, y), 0],
                                                                                    [self.v(x, y), 1 - self.v(x, y), 0],
                                                                                    [0, 0, (1 - 2 * self.v(x, y))/2]])
        else:
            print("Invalid stiffness matrix type (E_type) chosen. Must input either \'stress\' or \'strain\'")
            return None

    def D_lambda(self, x, y):
        return ((self.E(x, y) * self.v(x,y)) / ((1 + self.v(x,y))*(1 - 2 * self.v(x,y)))) * np.array([[1, 1, 0],
                                                                                                      [1, 1, 0],
                                                                                                      [0, 0, 0]])

    def D_mu(self, x, y):
        return (self.E(x, y) / (2 * (1 + self.v(x,y)))) * np.array([[2, 0, 0],
                                                                    [0, 2, 0],
                                                                    [0, 0, 1]])

    def display_mesh(self, title, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            if show_element_numbers:
                cent_x = np.sum(el_x) / len(el_x)
                cent_y = np.sum(el_y) / len(el_y)
                mesh_plot.text(cent_x, cent_y, str(i+1), color='black')

            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')

        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], str(i + 1), color='red', fontsize=10)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def lin_quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        return n_gp, xi, weights

    # def line_map_to_physical(self, xi, x1, x2):
    #     return (x2 + x1)/2 + xi*(x2-x1)/2

    def int_K(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.E_mat(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp, integrand)

    def int_K_SRI(self, n_gp_l, n_gp_m):
        for i in range(self.n_el):
            e = self.Elements[i]

            def int_lambda(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_lambda(x, y), e.B(xi, eta)))

            def int_mu(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_mu(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp_l, int_lambda) + e.gauss(n_gp_m, int_mu)


    def int_F_bod(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.N(xi, eta).T, self.b(x, y))

            e.el_F_bod = e.gauss(n_gp, integrand)

    def int_F_bound(self, n_gp):
        n_gp, xi, weights = self.lin_quad_selector(n_gp)
        for i in range(self.n_el):
            e = self.Elements[i]

            for j in range(e.edges):
                end_index = (j+2) % e.edges-1
                x_edge = np.array([e.x[j], e.x[end_index]])
                y_edge = np.array([e.y[j], e.y[end_index]])
                J = np.sqrt((x_edge[-1] - x_edge[0]) ** 2 + (y_edge[-1] - y_edge[0]) ** 2) / 2

                for k in range(n_gp):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    x, y = pos[0], pos[1]
                    e.el_F_bound += weights[k] * J * np.matmul(N.T,  self.t_bar(x, y))

    def assemble(self):
        self.K = np.zeros([self.DOF, self.DOF])
        self.F = np.zeros(self.DOF)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes*2):
                self.K[e.g_DOF[j], e.g_DOF] += e.el_K[j, :]
            self.F_bod[e.g_DOF] += e.el_F_bod
            self.F_bound[e.g_DOF] += e.el_F_bound
        self.F = self.F_bod + self.F_bound

    def solve_d(self):
        K_cond = self.K
        F_cond = self.F
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond -= self.u_bar(self.x[node], self.y[node])[i % 2] * K_cond[:, i]
                K_cond[i, :] = 0
                K_cond[:, i] = 0
                K_cond[i, i] = 1
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond[i] = self.u_bar(self.x[node], self.y[node])[i % 2]
        K_cond_sparse = sp.csr_matrix(K_cond)
        self.d = splg.spsolve(K_cond_sparse, F_cond)
        # self.d = np.matmul(np.linalg.inv(K_cond), F_cond)
        for i in range(self.n_el):
            self.Elements[i].d = self.d[self.Elements[i].g_DOF]

    def solution(self, x, y):
        for i in range(self.n_el):
            e = self.Elements[i]
            if e.in_element(x, y):
                return np.matmul(e.Nxy(x, y), e.d)
        # print("Position not in domain")
        return None

    def display_mesh_2(self, title, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt

        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)

                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')

            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        # mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
        #                   Line2D([0], [0], color='blue')],
        #                  ['Original mesh',
        #                   'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        # x_max, x_min = np.max(x_new), np.min(x_new)
        # mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def plot_displacement(self, title, scale, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers = True):
        mesh_plot = plt
        x_new = self.x + scale*self.d[0::2]
        y_new = self.y + scale*self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                x_pn = []
                y_pn = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale*d[0])
                    y_pn.append(pos[1] + scale*d[1])
                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')
                mesh_plot.plot(x_pn, y_pn, color='blue')
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')
            mesh_plot.scatter(x_new, y_new, color='blue')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
                          Line2D([0], [0], color='blue')],
                         ['Original mesh',
                          'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        x_max, x_min = np.max(x_new), np.min(x_new)
        mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def add_to_plot(self,
                    plot,
                    scale,
                    marker,
                    line_style,
                    legend,
                    color,
                    lines_per_edge=1,
                    show_nodes_numbers=True,
                    show_nodes=True,
                    show_element_numbers=True):

        mesh_plot = plot
        x_new = self.x + scale * self.d[0::2]
        y_new = self.y + scale * self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge + 1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):

                x_pn = []
                y_pn = []
                for k in range(lines_per_edge + 1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale * d[0])
                    y_pn.append(pos[1] + scale * d[1])
                mesh_plot.plot(x_pn, y_pn, color=color, linestyle=line_style)
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        if show_nodes:
            mesh_plot.scatter(x_new, y_new, marker=marker, color=color, linestyle=line_style, zorder=5)
            mesh_plot.plot([], [], marker=marker, color=color, label=legend, linestyle=line_style)
            # mesh_plot.scatter(x_new, y_new, marker=marker, color=color, label=legend, linestyle=line_style)
        # mesh_plot.legend.hanles.append(mlines.Line2D([], [], color=color, marker=marker, markersize=5, label=legend))

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "(" + str(i + 1) + ")", color='red', fontsize=10)

        return mesh_plot

    def plot_solution(self, x_res, y_res, title, type):
        x = np.linspace(np.min(self.x), np.max(self.x), x_res)
        y = np.linspace(np.min(self.y), np.max(self.y), y_res)
        x, y = np.meshgrid(x, y)
        z = np.zeros([y_res, x_res])
        for i in range(x_res):
            for j in range(y_res):
                z[j, i] = self.solution(x[j, i], y[j, i])

        masked_array = np.ma.array(z, mask=np.isnan(z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        if type == "smooth":
            c_plot = mesh_plot.pcolor(x, y, masked_array, cmap=cmap)
        elif type == "cont":
            c_plot = mesh_plot.contourf(x, y, masked_array, 10, cmap=cmap)
        else:
            print("Error. Invalid type chosen")
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / 3
            cent_y = np.sum(el_y) / 3
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()

    def plot(self, X, Y, Z):

        masked_array = np.ma.array(Z, mask=np.isnan(Z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        c_plot = mesh_plot.contourf(X, Y, masked_array, cmap=cmap)
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "["+str(i+1)+"]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title("FEM solution")
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()
    #
    # def plot_solution_element_wise(self, apprx_n_points):
    #     pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
    #     xi_pos = np.linspace(-1, 1, pp_iso)
    #     eta_pos = xi_pos
    #     Xs = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Ys = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Zs = np.zeros([self.n_el, pp_iso-1, pp_iso-1])
    #     for i in range(self.n_el):
    #         X = np.zeros([pp_iso, pp_iso])
    #         Y = np.zeros([pp_iso, pp_iso])
    #         Z = np.zeros([pp_iso-1, pp_iso-1])
    #         e = self.Elements[i]
    #         for j in range(pp_iso-1):
    #             for k in range(pp_iso-1):
    #                 X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
    #                 X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
    #                 X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
    #                 X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])
    #
    #                 Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
    #                 Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
    #                 Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
    #                 Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])
    #
    #                 xi_av = (xi_pos[j] + xi_pos[j+1])/2
    #                 eta_av = (eta_pos[k] + eta_pos[k+1])/2
    #
    #                 Z[j, k] = np.matmul(e.N(xi_av, eta_av), e.d)
    #         Xs[i] = X
    #         Ys[i] = Y
    #         Zs[i] = Z
    #     masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
    #     cmap = cm.jet
    #     cmap.set_bad('white', 1.)
    #
    #     mesh_plot = plt
    #     min_val, max_val = np.min(Zs), np.max(Zs)
    #
    #     for i in range(self.n_el):
    #         mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #         # mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #     mesh_plot.colorbar()
    #     for i in range(len(self.ICA)):
    #         el_nodes = self.ICA[i]
    #         el_x = self.x[el_nodes]
    #         el_y = self.y[el_nodes]
    #         cent_x = np.sum(el_x) / len(el_x)
    #         cent_y = np.sum(el_y) / len(el_y)
    #         mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
    #         mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')
    #
    #     mesh_plot.scatter(self.x, self.y, color='black')
    #
    #     mesh_plot.xlim(np.min(self.x), np.max(self.x))
    #     mesh_plot.ylim(np.min(self.y), np.max(self.y))
    #     mesh_plot.grid()
    #     mesh_plot.title("FEM solution")
    #     mesh_plot.xlabel("x (m)")
    #     mesh_plot.ylabel("y (m)")
    #     mesh_plot.show()
    #

    def plot_solution_element_wise(self, apprx_n_points, title, type):
        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])
        for i in range(self.n_el):
            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            e = self.Elements[i]
            for j in range(pp_iso):
                for k in range(pp_iso):
                    X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = np.matmul(e.N(xi_pos[j], eta_pos[k]), e.d)
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z
        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        min_val, max_val = np.min(Zs), np.max(Zs)

        for i in range(self.n_el):
            if type == "smooth":
                mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
            elif type == "cont":
                mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], np.linspace(min_val, np.around(max_val, -1), 15), vmin=min_val, vmax=max_val, cmap=cmap)
            else:
                print("Error. Invalid type chosen.")
        mesh_plot.colorbar()

        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()



    def plot_AB(self, res, type):
        x = np.linspace(np.min(self.x), 6, res)
        q = np.zeros([2, len(x)])
        T = np.zeros(len(x))
        for i in range(len(x)):
            for j in range(self.n_el):
                e = self.Elements[j]
                if e.in_element(x[i], 4):
                    q[:, i] = -np.matmul(self.D(x[i], 4), np.matmul(e.B(x[i], 4), e.d))
                    T[i] = np.matmul(e.Nxy(x[i], 4), e.d)
                    break
        if type == "heat":
            flux_plot = plt
            flux_plot.plot(x, q[0, :], label=r"$q_x$")
            flux_plot.plot(x, q[1, :], label=r"$q_y$")
            flux_plot.grid()
            flux_plot.xlabel(r"x $(cm)$")
            flux_plot.ylabel(r"Heat flux $(W)$")
            flux_plot.legend(loc='best')
            flux_plot.title(r"Heat flux along line AB")
            flux_plot.show()

        elif type == "temp":
            e = self.Elements[3]
            T_plot = plt
            T_plot.plot(x[0:len(x)-2], T[0:len(x)-2], label=r"$T$")
            T_plot.plot([0,4,6], [T[0],np.matmul(e.Nxy(4, 4), e.d),T[-2]], linewidth=0, marker='s', color='b')
            T_plot.grid()
            T_plot.xlabel(r"x $(cm)$")
            T_plot.ylabel(r"Temperature $(C)$")
            T_plot.title(r"Temperature along line AB")
            T_plot.show()



class Mixed_Mesh:
    """
    To Do:
        Adjust init
        int_K_bar *
        int_G *
        int_M *
        Assemble *
        Solve *
    """
    def __init__(self, x, y, ICA, E, v, t_bar, u_bar, b, BCs, E_type, thick, PCA, disp_element, pressure_element):

        self.x = x
        self.y = y
        self.pos = np.insert(y, np.arange(0, len(y)), x)
        self.BCs = BCs
        self.n_nodes = len(x)
        self.DOF = self.n_nodes * 2
        self.p_DOF = np.alen(PCA[0, :]) * np.alen(PCA[:, 0])
        self.ICA = ICA - 1
        self.PCA = PCA - 1
        self.n_el = len(ICA)
        self.K_bar = np.zeros([self.DOF, self.DOF])
        self.Y = np.zeros([self.n_nodes, self.n_nodes])
        self.G = np.zeros([self.DOF, self.p_DOF])
        self.GT = self.G.T
        self.M = np.zeros([self.p_DOF, self.p_DOF])
        self.K_big = np.zeros([self.DOF + self.p_DOF, self.DOF + self.p_DOF])
        self.F_big = np.zeros([self.DOF + self.p_DOF])
        self.F_tilde = np.zeros([self.p_DOF])
        self.d = np.zeros(self.DOF)
        self.p = np.zeros(self.p_DOF)
        self.F = np.zeros(self.DOF)
        self.P = np.zeros(self.n_nodes)
        self.P_tilde = np.zeros(self.n_nodes)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        self.t_bar = t_bar
        self.u_bar = u_bar
        self.b = b
        self.E = E
        self.v = v
        self.E_type = E_type
        self.thick = thick

        self.Elements = np.empty(self.n_el, dtype=me.QnPn)
        for i in range(self.n_el):
            local_nodes = self.ICA[i]
            self.Elements[i] = me.QnPn(x[local_nodes], y[local_nodes], local_nodes, i, self.PCA[i], disp_element, pressure_element)

    def E_mat(self, x, y):
        if self.E_type == "stress":
            return (self.E(x, y)/(1-self.v(x, y)**2)) * np.array([[1, self.v(x, y), 0],
                                                                  [self.v(x, y), 1, 0],
                                                                  [0, 0, (1 - self.v(x, y))/2]])
        elif self.E_type == "strain":
            return (self.E(x, y)/((1-2*self.v(x, y))*(1+self.v(x, y)))) * np.array([[1 - self.v(x, y), self.v(x, y), 0],
                                                                                    [self.v(x, y), 1 - self.v(x, y), 0],
                                                                                    [0, 0, (1 - 2 * self.v(x, y))/2]])
        else:
            print("Invalid stiffness matrix type (E_type) chosen. Must input either \'stress\' or \'strain\'")
            return None


    def D_lambda(self, x, y):
        return ((self.E(x, y) * self.v(x,y)) / ((1 + self.v(x,y))*(1 - 2 * self.v(x,y)))) * np.array([[1, 1, 0],
                                                                                                      [1, 1, 0],
                                                                                                      [0, 0, 0]])

    def lamba_neg1(self, x, y):
        return (1 + self.v(x,y))*(1 - 2 * self.v(x,y))/ (self.E(x, y) * self.v(x,y))

    def D_mu(self, x, y):
        return (self.E(x, y) / (2 * (1 + self.v(x,y)))) * np.array([[2, 0, 0],
                                                                    [0, 2, 0],
                                                                    [0, 0, 1]])

    def display_mesh(self, title, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            if show_element_numbers:
                cent_x = np.sum(el_x) / len(el_x)
                cent_y = np.sum(el_y) / len(el_y)
                mesh_plot.text(cent_x, cent_y, str(i+1), color='black')

            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')

        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], str(i + 1), color='red', fontsize=10)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def lin_quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        return n_gp, xi, weights

    # def line_map_to_physical(self, xi, x1, x2):
    #     return (x2 + x1)/2 + xi*(x2-x1)/2
    def int_K_bar(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_mu(x, y), e.B(xi, eta)))

            e.K_bar = e.gauss(n_gp, integrand)
            # print("Element ", i, "\n", e.K_bar)
            #

            # print("Element ", i, "Kbar \n", e.K_bar)
            # print("Element ", i, "Kbar - Kbar^T  \n", e.K_bar - e.K_bar.T)
            #
            # plt.spy(e.K_bar)
            # plt.show()
            #
            # plt.spy(e.K_bar - e.K_bar.T)
            # plt.show()

    def int_G(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                #
                # print(self.thick(x, y))
                # print(e.L(xi, eta))
                # print(e.N_tilde(xi, eta))
                # print(np.dot(np.array([e.L(xi, eta)]).transpose(), np.array([e.N_tilde(xi, eta)])))
                #
                # if np.alen(e.N_tilde(xi, eta)) == 1:
                #     # print(self.thick(x, y))
                #     # print(e.L(xi, eta))
                #     # print(e.N_tilde(xi, eta))
                #     # print(e.N_tilde(xi, eta)[0])
                #     # print(- self.thick(x, y) * e.L(xi, eta) * e.N_tilde(xi, eta)[0])
                #     return - self.thick(x, y) * e.L(xi, eta) * e.N_tilde(xi, eta)[0]
                # else:
                return - self.thick(x, y) * np.matmul(np.array([e.L(xi, eta)]).transpose(), np.array([e.N_tilde(xi, eta)]))

            e.G = e.gauss(n_gp, integrand)
            # print("e.G", e.G)
            s=5

    def int_M(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return - self.thick(x, y) * self.lamba_neg1(x, y) * np.matmul(np.array([e.N_tilde(xi, eta)]).T, np.array([e.N_tilde(xi, eta)]))

            e.M = e.gauss(n_gp, integrand)

    def int_Y(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(np.array([e.N_scalar(xi, eta)]).T, np.array([e.N_scalar(xi, eta)]))

            e.Y = e.gauss(n_gp, integrand)

    def int_P_cont(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * e.N_scalar(xi, eta) * e.p

            e.P_cont = e.gauss(n_gp, integrand)

    def int_K(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.E_mat(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp, integrand)


    def int_K_SRI(self, n_gp_l, n_gp_m):
        for i in range(self.n_el):
            e = self.Elements[i]

            def int_lambda(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_lambda(x, y), e.B(xi, eta)))

            def int_mu(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.B(xi, eta).T, np.matmul(self.D_mu(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp_l, int_lambda) + e.gauss(n_gp_m, int_mu)


    def int_F_bod(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                pos = e.iso_to_physical(xi, eta)
                x, y = pos[0], pos[1]
                return self.thick(x, y) * np.matmul(e.N(xi, eta).T, self.b(x, y))

            e.el_F_bod = e.gauss(n_gp, integrand)

    def int_F_bound(self, n_gp):
        n_gp, xi, weights = self.lin_quad_selector(n_gp)
        for i in range(self.n_el):
            e = self.Elements[i]

            for j in range(e.edges):
                end_index = (j+2) % e.edges-1
                x_edge = np.array([e.x[j], e.x[end_index]])
                y_edge = np.array([e.y[j], e.y[end_index]])
                J = np.sqrt((x_edge[-1] - x_edge[0]) ** 2 + (y_edge[-1] - y_edge[0]) ** 2) / 2

                for k in range(n_gp):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    x, y = pos[0], pos[1]
                    e.el_F_bound += weights[k] * J * np.matmul(N.T,  self.t_bar(x, y))
                s=5

    def assemble_Y_P(self):
        # self.Y = np.zeros([self.DOF, self.DOF])
        # self.P = np.zeros(self.DOF)

        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes):
                self.Y[e.g_nodes[j], e.g_nodes] += e.Y[j, :]

            self.P[e.g_nodes] += e.P_cont

    def assemble(self):
        self.K_bar = np.zeros([self.DOF, self.DOF])
        self.F = np.zeros(self.DOF)
        self.F_bod = np.zeros(self.DOF)
        self.F_bound = np.zeros(self.DOF)
        for i in range(self.n_el):
            # plt.spy(self.K_bar)
            # plt.spy(self.G)
            # plt.spy(self.M)
            # plt.grid()
            # plt.show()
            e = self.Elements[i]
            for j in range(e.n_nodes*2):
                self.K_bar[e.g_DOF[j], e.g_DOF] += e.K_bar[j, :]
                # print(self.G[e.g_DOF[j], e.p_node_numbers])
                # print(e.G)

                if (np.alen(e.G) == e.disp_dofs):
                    self.G[e.g_DOF[j], e.p_node_numbers] += e.G[j]


                else:
                    self.G[e.g_DOF[j], e.p_node_numbers] += e.G[j, :]
            for j in range(e.p_nodes):
                if e.p_nodes > 1:
                    self.M[e.p_node_numbers[j], e.p_node_numbers] += e.M[j, :]
                else:
                    self.M[e.p_node_numbers, e.p_node_numbers] += e.M[0]
                # plt.spy(self.M)
                # plt.grid()
                # plt.show()

            self.F_bod[e.g_DOF] += e.el_F_bod
            self.F_bound[e.g_DOF] += e.el_F_bound
        self.F = self.F_bod + self.F_bound

        # plt.spy(self.M)
        #
        # #
        # # plt.spy(self.G)
        # # plt.show()
        # #
        # # plt.spy(self.K_bar)
        # plt.grid()
        # plt.show()
        self.K_big[0:self.DOF, 0:self.DOF] = self.K_bar
        self.K_big[0:self.DOF, self.DOF:] = self.G
        self.K_big[self.DOF:, 0:self.DOF] = self.G.T
        self.K_big[self.DOF:, self.DOF:] = self.M

        self.F_big[:self.DOF] = self.F

    def solve_p(self):
        self.P_tilde = np.matmul(np.linalg.inv(self.Y), self.P)
        for i in range(self.n_el):
            self.Elements[i].P_cont_tilde = self.P_tilde[self.Elements[i].g_nodes]

    def solve_d(self):
        # plt.spy(self.K_big)
        # plt.show()
        # plt.spy(self.K_big - self.K_big.T)
        # plt.show()
        F_cond = self.F_big
        K_cond = self.K_big
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond -= self.u_bar(self.x[node], self.y[node])[i % 2] * K_cond[:, i]
                K_cond[i, :] = 0
                K_cond[:, i] = 0
                K_cond[i, i] = 1
        for i in range(self.DOF):
            node = int(i/2)
            if self.BCs[i] == 1:
                F_cond[i] = self.u_bar(self.x[node], self.y[node])[i % 2]

        self.K_bar = K_cond[0:self.DOF, 0:self.DOF]
        self.G = K_cond[0:self.DOF, self.DOF:]
        self.GT = K_cond[self.DOF:, 0:self.DOF]
        self.M = K_cond[self.DOF:, self.DOF:]

        self.F = F_cond[:self.DOF]
        self.F_tilde = F_cond[self.DOF:]

        K_bar = self.K_bar
        G = self.G
        GT = self.GT
        M = self.M
        F_bar = self.F
        F_tilde = self.F_tilde

        Kinv = np.linalg.inv(K_bar)

        self.p = np.matmul(np.linalg.inv(M - np.matmul(GT, np.matmul(Kinv, G))), (F_tilde - np.matmul(GT, np.matmul(Kinv, F_bar))))
        self.d = np.matmul(Kinv, (F_bar - np.matmul(G, self.p)))

        # K_cond_sparse = sp.csr_matrix(K_cond)
        # self.d = splg.spsolve(K_cond_sparse, F_cond)
        # self.d = np.matmul(np.linalg.inv(K_cond), F_cond)
        for i in range(self.n_el):
            self.Elements[i].d = self.d[self.Elements[i].g_DOF]
            self.Elements[i].p = self.p[self.Elements[i].p_node_numbers]

    def solution(self, x, y):
        for i in range(self.n_el):
            e = self.Elements[i]
            if e.in_element(x, y):
                return np.matmul(e.Nxy(x, y), e.d)
        # print("Position not in domain")
        return None

    def display_mesh_2(self, title, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers=True):
        mesh_plot = plt

        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)

                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')

            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        # mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
        #                   Line2D([0], [0], color='blue')],
        #                  ['Original mesh',
        #                   'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        # x_max, x_min = np.max(x_new), np.min(x_new)
        # mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def plot_displacement(self, title, scale, lines_per_edge=1, show_nodes_numbers=True, show_nodes=True, show_element_numbers = True):
        mesh_plot = plt
        x_new = self.x + scale*self.d[0::2]
        y_new = self.y + scale*self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge+1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):
                x_p = []
                y_p = []
                x_pn = []
                y_pn = []
                for k in range(lines_per_edge+1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale*d[0])
                    y_pn.append(pos[1] + scale*d[1])
                    x_p.append(pos[0])
                    y_p.append(pos[1])
                mesh_plot.plot(x_p, y_p, color='black', linestyle='--')
                mesh_plot.plot(x_pn, y_pn, color='blue')
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "["+str(i + 1)+"]", color='black')


        if show_nodes:
            mesh_plot.scatter(self.x, self.y, color='black')
            mesh_plot.scatter(x_new, y_new, color='blue')

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "("+str(i + 1)+")", color='red', fontsize=10)

        mesh_plot.legend([Line2D([0], [0], color='black', linestyle='--'),
                          Line2D([0], [0], color='blue')],
                         ['Original mesh',
                          'Displaced mesh\nscale = ' + str(scale)], loc='center right')
        x_max, x_min = np.max(x_new), np.min(x_new)
        mesh_plot.xlim(x_min - 0.1*(x_max - x_min), x_max + 0.5*(x_max - x_min))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def add_to_plot(self,
                    plot,
                    scale,
                    marker,
                    line_style,
                    legend,
                    color,
                    lines_per_edge=1,
                    show_nodes_numbers=True,
                    show_nodes=True,
                    show_element_numbers=True):


        mesh_plot = plot
        x_new = self.x + scale * self.d[0::2]
        y_new = self.y + scale * self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge + 1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):

                x_pn = []
                y_pn = []
                for k in range(lines_per_edge + 1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale * d[0])
                    y_pn.append(pos[1] + scale * d[1])
                mesh_plot.plot(x_pn, y_pn, color=color, linestyle=line_style)
            if show_element_numbers:
                cent_x = np.sum(e.x) / len(e.x)
                cent_y = np.sum(e.y) / len(e.y)
                mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        if show_nodes:
            mesh_plot.scatter(x_new, y_new, marker=marker, color=color, zorder=5)
            mesh_plot.plot([], [], marker=marker, color=color, label=legend, linestyle=line_style)

        if show_nodes_numbers:
            for i in range(self.n_nodes):
                mesh_plot.text(self.x[i], self.y[i], "(" + str(i + 1) + ")", color='red', fontsize=10)

        return mesh_plot

    def plot_solution(self, x_res, y_res, title, type):
        x = np.linspace(np.min(self.x), np.max(self.x), x_res)
        y = np.linspace(np.min(self.y), np.max(self.y), y_res)
        x, y = np.meshgrid(x, y)
        z = np.zeros([y_res, x_res])
        for i in range(x_res):
            for j in range(y_res):
                z[j, i] = self.solution(x[j, i], y[j, i])

        masked_array = np.ma.array(z, mask=np.isnan(z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        if type == "smooth":
            c_plot = mesh_plot.pcolor(x, y, masked_array, cmap=cmap)
        elif type == "cont":
            c_plot = mesh_plot.contourf(x, y, masked_array, 10, cmap=cmap)
        else:
            print("Error. Invalid type chosen")
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / 3
            cent_y = np.sum(el_y) / 3
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()

    def plot(self, X, Y, Z):

        masked_array = np.ma.array(Z, mask=np.isnan(Z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        c_plot = mesh_plot.contourf(X, Y, masked_array, cmap=cmap)
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "["+str(i+1)+"]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title("FEM solution")
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()
    #
    # def plot_solution_element_wise(self, apprx_n_points):
    #     pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
    #     xi_pos = np.linspace(-1, 1, pp_iso)
    #     eta_pos = xi_pos
    #     Xs = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Ys = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Zs = np.zeros([self.n_el, pp_iso-1, pp_iso-1])
    #     for i in range(self.n_el):
    #         X = np.zeros([pp_iso, pp_iso])
    #         Y = np.zeros([pp_iso, pp_iso])
    #         Z = np.zeros([pp_iso-1, pp_iso-1])
    #         e = self.Elements[i]
    #         for j in range(pp_iso-1):
    #             for k in range(pp_iso-1):
    #                 X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
    #                 X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
    #                 X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
    #                 X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])
    #
    #                 Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
    #                 Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
    #                 Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
    #                 Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])
    #
    #                 xi_av = (xi_pos[j] + xi_pos[j+1])/2
    #                 eta_av = (eta_pos[k] + eta_pos[k+1])/2
    #
    #                 Z[j, k] = np.matmul(e.N(xi_av, eta_av), e.d)
    #         Xs[i] = X
    #         Ys[i] = Y
    #         Zs[i] = Z
    #     masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
    #     cmap = cm.jet
    #     cmap.set_bad('white', 1.)
    #
    #     mesh_plot = plt
    #     min_val, max_val = np.min(Zs), np.max(Zs)
    #
    #     for i in range(self.n_el):
    #         mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #         # mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #     mesh_plot.colorbar()
    #     for i in range(len(self.ICA)):
    #         el_nodes = self.ICA[i]
    #         el_x = self.x[el_nodes]
    #         el_y = self.y[el_nodes]
    #         cent_x = np.sum(el_x) / len(el_x)
    #         cent_y = np.sum(el_y) / len(el_y)
    #         mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
    #         mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')
    #
    #     mesh_plot.scatter(self.x, self.y, color='black')
    #
    #     mesh_plot.xlim(np.min(self.x), np.max(self.x))
    #     mesh_plot.ylim(np.min(self.y), np.max(self.y))
    #     mesh_plot.grid()
    #     mesh_plot.title("FEM solution")
    #     mesh_plot.xlabel("x (m)")
    #     mesh_plot.ylabel("y (m)")
    #     mesh_plot.show()
    #

    def plot_solution_element_wise(self, apprx_n_points, title, type):
        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])
        for i in range(self.n_el):
            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            e = self.Elements[i]
            for j in range(pp_iso):
                for k in range(pp_iso):
                    X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = np.matmul(e.N(xi_pos[j], eta_pos[k]), e.d)
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z
        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        min_val, max_val = np.min(Zs), np.max(Zs)

        for i in range(self.n_el):
            if type == "smooth":
                mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
            elif type == "cont":
                mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], np.linspace(min_val, np.around(max_val, -1), 15), vmin=min_val, vmax=max_val, cmap=cmap)
            else:
                print("Error. Invalid type chosen.")
        mesh_plot.colorbar()

        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()

    def plot_discontinuous_pressure(self, title, xlabel, ylabel, apprx_n_points, scale, lines_per_edge, marker, color, legend, line_style, show_nodes=True):

        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])

        mesh_plot = plt
        x_new = self.x + scale * self.d[0::2]
        y_new = self.y + scale * self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge + 1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):

                x_pn = []
                y_pn = []
                for k in range(lines_per_edge + 1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale * d[0])
                    y_pn.append(pos[1] + scale * d[1])
                mesh_plot.plot(x_pn, y_pn, color=color, linestyle=line_style, zorder=5)

            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            for j in range(pp_iso):
                for k in range(pp_iso):
                    N = e.N(xi_pos[j], eta_pos[k])

                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)

                    X[j, k] = pos[0] + scale * d[0]
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = pos[1] + scale * d[1]
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = e.p[0]
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z

        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        min_val, max_val = np.min(Zs), np.max(Zs)
        for i in range(self.n_el):
            mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
        mesh_plot.colorbar()

        if show_nodes:
            mesh_plot.scatter(x_new, y_new, marker=marker, color=color, zorder=5)
            mesh_plot.plot([], [], marker=marker, color=color, label=legend, linestyle=line_style)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel(xlabel)
        mesh_plot.ylabel(ylabel)
        mesh_plot.show()


    def plot_continuous_pressure(self, title, xlabel, ylabel, apprx_n_points, scale, lines_per_edge, marker, color, legend, line_style, show_nodes=True):

        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])

        mesh_plot = plt
        x_new = self.x + scale * self.d[0::2]
        y_new = self.y + scale * self.d[1::2]
        xi = np.linspace(-1, 1, lines_per_edge + 1)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.edges):

                x_pn = []
                y_pn = []
                for k in range(lines_per_edge + 1):
                    N = e.N_edge(j, xi[k])
                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)
                    x_pn.append(pos[0] + scale * d[0])
                    y_pn.append(pos[1] + scale * d[1])
                mesh_plot.plot(x_pn, y_pn, color=color, linestyle=line_style, zorder=5)

            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            for j in range(pp_iso):
                for k in range(pp_iso):
                    N = e.N(xi_pos[j], eta_pos[k])

                    pos = np.dot(N, e.pos)
                    d = np.matmul(N, e.d)

                    X[j, k] = pos[0] + scale * d[0]
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = pos[1] + scale * d[1]
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = e.P_tilde(xi_pos[j], eta_pos[k])
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z

        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        min_val, max_val = np.min(Zs), np.max(Zs)
        for i in range(self.n_el):
            mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
        mesh_plot.colorbar()

        if show_nodes:
            mesh_plot.scatter(x_new, y_new, marker=marker, color=color, zorder=5)
            mesh_plot.plot([], [], marker=marker, color=color, label=legend, linestyle=line_style)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel(xlabel)
        mesh_plot.ylabel(ylabel)
        mesh_plot.show()




    def plot_AB(self, res, type):
        x = np.linspace(np.min(self.x), 6, res)
        q = np.zeros([2, len(x)])
        T = np.zeros(len(x))
        for i in range(len(x)):
            for j in range(self.n_el):
                e = self.Elements[j]
                if e.in_element(x[i], 4):
                    q[:, i] = -np.matmul(self.D(x[i], 4), np.matmul(e.B(x[i], 4), e.d))
                    T[i] = np.matmul(e.Nxy(x[i], 4), e.d)
                    break
        if type == "heat":
            flux_plot = plt
            flux_plot.plot(x, q[0, :], label=r"$q_x$")
            flux_plot.plot(x, q[1, :], label=r"$q_y$")
            flux_plot.grid()
            flux_plot.xlabel(r"x $(cm)$")
            flux_plot.ylabel(r"Heat flux $(W)$")
            flux_plot.legend(loc='best')
            flux_plot.title(r"Heat flux along line AB")
            flux_plot.show()

        elif type == "temp":
            e = self.Elements[3]
            T_plot = plt
            T_plot.plot(x[0:len(x)-2], T[0:len(x)-2], label=r"$T$")
            T_plot.plot([0,4,6], [T[0],np.matmul(e.Nxy(4, 4), e.d),T[-2]], linewidth=0, marker='s', color='b')
            T_plot.grid()
            T_plot.xlabel(r"x $(cm)$")
            T_plot.ylabel(r"Temperature $(C)$")
            T_plot.title(r"Temperature along line AB")
            T_plot.show()

