import numpy as np
import continuous_2D_vector_elements as ct
import discontinuous_2D_scalar_elements as dt
#
# class Q1P0:
#     def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
#         self.Q1 = ct.Q1(x_nodes, y_nodes, g_node_numbers, el_num)
#         self.P0 = dt.P0
#         self.K_bar = np.empty([8, 8], dtype=float)
#         self.G = np.empty([8, 1], dtype=float)
#         self.M = np.empty([1, 1], dtype=float)
#         self.K_bar = np.empty([8, 8], dtype=float)
#


class QnPn:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num, p_node_numbers, disp_el, press_el):
        self.disp_el = disp_el(x_nodes, y_nodes, g_node_numbers, el_num)
        self.press_el = press_el(p_node_numbers)

        self.n_nodes = self.disp_el.n_nodes     # The number of nodes that the element has
        self.edges = self.disp_el.edges      # The number of edges that the element has
        self.x = self.disp_el.x  # Assigning the x positions of the nodes
        self.y = y_nodes    # Assigning the y positions of the nodes
        self.pos = self.disp_el.pos
        self.g_nodes = g_node_numbers   # Assigning the global node numbers
        self.g_DOF = self.disp_el.g_DOF# Creating an array of degree of freedom numbers
        self.el_num = self.disp_el.el_num    # Assigning the element number
        self.el_F = self.disp_el.el_F# Initialising an empty element force vector
        self.el_F_bound = self.disp_el.el_F_bound # Initialising an empty element boundary force vector
        self.el_F_bod = self.disp_el.el_F_bod       # Initialising an empty element body force vector
        self.d = self.disp_el.d                # Initialising an empty element displacement vector

        self.B = self.disp_el.B
        self.N = self.disp_el.N
        try:
            self.N_scalar = self.disp_el.N_scalar
        except:
            self.N_scalar= np.zeros(3)
        self.N_edge = self.disp_el.N_edge
        self.gauss = self.disp_el.gauss
        self.iso_to_physical = self.disp_el.iso_to_physical

        self.p_nodes = self.press_el.nodes
        self.p = self.press_el.p
        self.p_node_numbers = self.press_el.node_numbers
        self.N_tilde = self.press_el.N_tilde


        # self.Q1 = disp_el(x_nodes, y_nodes, g_node_numbers, el_num)
        # self.P0 = dt.P0
        disp_dofs = self.disp_el.n_nodes*2
        press_dofs = self.press_el.nodes
        self.disp_dofs = disp_dofs
        self.press_dofs = press_dofs

        self.K_bar = np.zeros([disp_dofs, disp_dofs], dtype=float)
        self.G = np.zeros([disp_dofs, press_dofs], dtype=float)
        self.M = np.zeros([press_dofs, press_dofs], dtype=float)

        self.Y = np.zeros([self.n_nodes, self.n_nodes], dtype=float)
        self.P_cont = np.zeros([self.n_nodes], dtype=float)
        self.P_cont_tilde = np.zeros([self.n_nodes], dtype=float)


    def L(self, xi, eta):
        return self.disp_el.B(xi, eta)[0, :] + self.disp_el.B(xi, eta)[1, :]

    def P_tilde(self, xi, eta):
        return np.dot(self.N_scalar(xi, eta), self.P_cont_tilde)