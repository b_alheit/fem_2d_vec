\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{MEC5064Z Project One}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 1in]{geometry}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
\bibliography{ref.bib}
%\usepackage[nottoc]{tocbibind}


\begin{document}
\maketitle
\section*{Question 1}
\subsection*{Finding $\bm{f}$}
The traction, $\bm{f}$ can be written as
\begin{equation}
\bm{f}(y) = c\left(\frac{l}{2}-y\right) \left[\begin{array}{c}
1 \\
0
\end{array}\right]
\end{equation}
where $c$ is some constant that describes the slope of the traction in the $y$ direction. To find the value of $c$ in terms of the maximum value of the traction, $f_{max}$, the traction is set to have its maximum value at $y=0$. 
\begin{equation}
\begin{gathered}
f_1(0) = c\left(\frac{l}{2}-0\right) = f_{max}\\
\Rightarrow c = \frac{2f_{max}}{l}
\end{gathered}
\end{equation}
Therefore, $\bm{f}$ can be written as
\begin{equation}
\bm{f}(y) = \frac{2f_{max}}{l}\left(\frac{l}{2}-y\right)\left[\begin{array}{c}
1\\
0
\end{array}\right].
\end{equation}
\subsection*{Analysing the exact solutions}
To show that the exact solutions, 
\begin{equation}
\begin{gathered}
u_x(x,y) = \frac{2f_{max}(1-\nu^2)}{El}x\left(\frac{l}{2}-y\right),\\
u_y(x,y) = \frac{f_{max}(1-\nu^2)}{El}\left(x^2 + \frac{\nu}{1-\nu}y\left(y-l\right)\right),
\end{gathered}
\end{equation}
satisfy the equilibrium and boundary conditions, the governing equations of linear elasticity will be used. Recall that these are
\begin{equation}
\begin{gathered}
\nabla_s^T \bm{\sigma} + \bm{b} = \bm{0}, \\
\bm{\sigma} = \bm{D} \bm{\epsilon}, \\
\bm{\epsilon} = \nabla_s \bm{u},
\end{gathered}
\end{equation}
where $\nabla_s^T$ is the symmetric gradient operator, $\bm{\sigma}$ is the 2D stress vector (in Voigt notation), $\bm{b}$ is the body force, $\bm{D}$ is the (plane strain) elasticity tensor, $\bm{\epsilon}$ is the 2D stress vector (also in Voigt notation) and $\bm{u}$ is the displacement. 

\subsubsection*{Displacement boundary conditions}
The displacement boundary conditions are
\begin{equation}
\begin{gathered}
\overline{u_x} = 0, \qquad x = 0, 0 \leq y \leq l \\
\overline{u_y} = 0, \qquad x = y = 0.
\end{gathered}
\end{equation}
These are satisfied by the analytical solutions since 
\begin{equation}
\begin{gathered}
u_x(0,y) = \frac{2f_{max}(1-\nu^2)}{El}(0)\left(\frac{l}{2}-y\right) = 0 = \overline{u_x}\\
u_y(0, 0) = \frac{f_{max}(1-\nu^2)}{El}\left((0)^2 + \frac{\nu}{1-\nu}(0)\left((0)-l\right)\right) = 0 = \overline{u_y}.
\end{gathered}
\end{equation}

\subsubsection*{Traction boundary conditions}
The traction boundary conditions are 
\begin{equation}
\begin{gathered}
\bm{\overline{t}} = \bm{\overline{t}}\big|_R = \bm{f} \qquad x=L, 0 \leq  y \leq l \\
\bm{\overline{t}} = \bm{\overline{t}}\big|_T = \bm{0} \qquad 0 \leq x \leq L, y = l \\ 
\bm{\overline{t}} = \bm{\overline{t}}\big|_B = \bm{0} \qquad 0 \leq x \leq L, y = 0 \\
\overline{t}_y = \overline{t}_y\big|_L = 0 \qquad x = 0, 0 \leq y \leq l. \\
\end{gathered}
\end{equation}
The tractions at the relevant boundaries will be verified by investigating $(\bm{\tau n})\big|_{boundary}$. Where
\begin{equation}
\bm{\tau} = \left[ \begin{array}{c c}
\sigma_{xx} & \sigma_{xy} \\
\sigma_{xy} & \sigma_{yy}
\end{array} \right],
\end{equation}
and 
\begin{equation}
\bm{\sigma} = \left[ \begin{array}{c}
\sigma_{xx} \\
\sigma_{yy} \\
\sigma_{xy}
\end{array} \right].
\end{equation}
To do this we start by expanding $\bm{\epsilon}$ and $\bm{D}$ 
\begin{equation}
\begin{gathered}
\bm{\epsilon} = \left[\begin{array}{c} \frac{\partial u_x}{\partial x}\\
\frac{\partial u_y}{\partial y} \\
\frac{\partial u_x}{\partial y} + \frac{\partial u_y}{\partial x}
\end{array}\right],\\
\bm{D} = \frac{E}{(1+\nu)(1-2\nu)}\left[ \begin{array}{c c c} 
1-\nu & \nu & 0 \\
\nu  & 1-\nu & 0 \\
0 & 0 & \frac{1-2\nu}{2}
\end{array}\right]. \\
\end{gathered}
\end{equation}
Multiplying $\bm{\epsilon}$ into $\bm{D}$ to get $\bm{\sigma}$ yields
\begin{equation}
\begin{gathered}
\bm{\sigma} = \frac{E}{(1+\nu)(1-2\nu)} \left[\begin{array}{c}
(1-\nu)\frac{\partial u_x}{\partial x} + \nu \frac{\partial u_y}{\partial y} \\
\nu  \frac{\partial u_x}{\partial x} + (1-\nu) \frac{\partial u_y}{\partial y}\\
(\frac{1-2\nu}{2})(\frac{\partial u_x}{\partial y} + \frac{\partial u_y}{\partial x})
\end{array}\right].
\end{gathered}
\label{eq:sigma}
\end{equation}
The partial derivatives of the displacement are then evaluated
\begin{equation}
\begin{gathered}
\frac{\partial u_x}{\partial x} = \frac{\partial }{\partial x} \Bigg[\frac{2f_{max}(1-\nu^2)}{El} x \bigg(\frac{l}{2}-y\bigg)\Bigg] = \frac{2f_{max}(1-\nu^2)}{El} \bigg(\frac{l}{2}-y\bigg),\\
\frac{\partial u_x}{\partial y} = \frac{\partial }{\partial y} \Bigg[\frac{2f_{max}(1-\nu^2)}{El} x \bigg(\frac{l}{2}-y\bigg)\Bigg] = -\frac{2f_{max}(1-\nu^2)}{El}x, \\
\frac{\partial u_y}{\partial y} = \frac{\partial }{\partial y} \Bigg[\frac{f_{max}(1-\nu^2)}{El} \bigg(x^2 + \frac{\nu}{1-\nu}y(y-l)\bigg)\Bigg] = -2\frac{f_{max}(1-\nu^2)}{El}\frac{\nu}{1-\nu}\left(\frac{l}{2}-y\right),\\
\frac{\partial u_y}{\partial x} = \frac{\partial }{\partial x} \Bigg[\frac{f_{max}(1-\nu^2)}{El} \bigg(x^2 + \frac{\nu}{1-\nu}y(y-l)\bigg)\Bigg] = 2\frac{f_{max}(1-\nu^2)}{El}x.
\end{gathered}
\end{equation}
To evaluate $\sigma_{xx}$ the relevant partial derivatives are substituted into the expression for $\sigma_{xx}$ in equation \ref{eq:sigma}. If the coefficient is ignored for now the expression can be simplified as follows
\begin{equation}
\begin{gathered}
(1-\nu)\frac{\partial u_x}{\partial x} + \nu \frac{\partial u_y}{\partial y} = (1-\nu)\frac{2f_{max}(1-\nu^2)}{El} \bigg(\frac{l}{2}-y\bigg)  -2\nu\frac{f_{max}(1-\nu^2)}{El}\frac{\nu}{1-\nu}(\frac{l}{2}-y) =\\ \frac{2f_{max}(1-\nu^2)}{El} \bigg(\frac{l}{2}-y\bigg) ((1-\nu) - \frac{\nu^2}{1-\nu}) = \frac{2f_{max}(1-\nu^2)}{El} \bigg(\frac{l}{2}-y\bigg) \frac{1 - 2\nu}{1-\nu} = \\
\frac{2f_{max}(1+\nu)(1 - 2\nu)}{El} \bigg(\frac{l}{2}-y\bigg).
\end{gathered}
\end{equation}
Hence, $\sigma_{xx}$ can be evaluated by multiplying in the coefficient
\begin{equation}
\begin{gathered}
\sigma_{xx} = \frac{E}{(1+\nu)(1-2\nu)}\frac{2f_{max}(1+\nu)(1 - 2\nu)}{El} \bigg(\frac{l}{2}-y\bigg) = \frac{2f_{max}}{l}\bigg(\frac{l}{2}-y\bigg).
\end{gathered}
\end{equation}
The remaining components of $\bm{\sigma}$ can be evaluated by substituting the relative partial derivatives into equation \ref{eq:sigma}. Since the remaining components of $\bm{\sigma}$ go to $0$ the coefficient of $\bm{\sigma}$ need not be considered.
\begin{equation}
\begin{gathered}
\nu  \frac{\partial u_x}{\partial x} + (1-\nu) \frac{\partial u_y}{\partial y} = \nu  \frac{2f_{max}(1-\nu^2)}{El} \bigg(\frac{l}{2}-y\bigg)  -2(1-\nu)\frac{f_{max}(1-\nu^2)}{El}\frac{\nu}{1-\nu}\left(\frac{l}{2}-y\right) = 0\\
\Rightarrow \sigma_{yy} = 0 \\
\frac{\partial u_x}{\partial y} + \frac{\partial u_y}{\partial x} = -\frac{2f_{max}(1-\nu^2)}{El}x + 2\frac{f_{max}(1-\nu^2)}{El}x = 0\\
\Rightarrow \sigma_{xy} = 0 
\end{gathered}
\end{equation}
Hence the stress tensor, $\bm{\tau}$, can be found as
\begin{equation}
\bm{\tau} = \left[\begin{array}{c c}
\sigma_{xx} & \sigma_{xy} \\
\sigma_{xy} & \sigma_{yy} 
\end{array} \right] = \frac{2f_{max}}{l}\bigg(\frac{l}{2}-y\bigg)\left[\begin{array}{c c}
1 & 0 \\
0 & 0
\end{array} \right].
\end{equation}
The traction boundary conditions on the relevant faces can then be evaluated as follows
\begin{equation}
\begin{gathered}
\bm{t}\big|_R = \bm{\tau n}\big|_R =\frac{2f_{max}}{l}\bigg(\frac{l}{2}-y\bigg)\left[\begin{array}{c c}
1 & 0 \\
0 & 0
\end{array} \right] \left[\begin{array}{c}
1 \\
0
\end{array} \right]  =\frac{2f_{max}}{l}\bigg(\frac{l}{2}-y\bigg)\left[\begin{array}{c}
1 \\
0 
\end{array} \right] = \bm{f} = \bm{\overline{t}}\big|_R \\
\bm{t}\big|_T = \bm{\tau n}\big|_T =\frac{2f_{max}}{l}\bigg(\frac{l}{2}-l\bigg)\left[\begin{array}{c c}
1 & 0 \\
0 & 0
\end{array} \right] \left[\begin{array}{c}
0 \\
1
\end{array} \right] = \bm{0} =\bm{\overline{t}}\big|_T \\
\bm{t}\big|_B = \bm{\tau n}\big|_B =\frac{2f_{max}}{l}\bigg(\frac{l}{2}-0\bigg)\left[\begin{array}{c c}
1 & 0 \\
0 & 0
\end{array} \right] \left[\begin{array}{c}
0 \\
-1
\end{array} \right] = \bm{0} = \bm{\overline{t}}\big|_B \\
t_y\big|_L = \bm{\tau_y n}\big|_L =\frac{2f_{max}}{l}\bigg(\frac{l}{2}-y\bigg)\left[\begin{array}{c c}
0 & 0
\end{array} \right] \left[\begin{array}{c}
-1 \\
0
\end{array} \right] = 0 = \overline{t}_y\big|_L.
\end{gathered}
\end{equation}
Hence, the provided exact solutions satisfies all traction boundary conditions.
\subsubsection*{Equilibrium equation}
Since there are no body forces, the equilibrium equation reduces to
\begin{equation}
\nabla_s^T\bm{\sigma} = \bm{0}
\end{equation}
Expanding $\bm{\sigma}$ yields
\begin{equation}
\begin{gathered}
\nabla_s^T\bm{\sigma}  = \frac{E}{(1+\nu)(1-2\nu)} \nabla_s^T \left[\begin{array}{c}
(1-\nu)\frac{\partial u_x}{\partial x} + \nu \frac{\partial u_y}{\partial y} \\
\nu  \frac{\partial u_x}{\partial x} + (1-\nu) \frac{\partial u_y}{\partial y}\\
(\frac{1-2\nu}{2})(\frac{\partial u_x}{\partial y} + \frac{\partial u_y}{\partial x})
\end{array}\right] = \bm{0}.
\end{gathered}
\end{equation}
The coefficient of $\bm{\sigma}$ need not be considered due to the zero on the right hand side. An expression for the first component of the resulting force vector can be determined by multiplying $\bm{\sigma}$ into the symmetric gradient operator and simplifying as follows
\begin{equation}
\begin{gathered}
(1-\nu)\frac{\partial^2 u_x}{\partial x^2} + \nu \frac{\partial^2 u_y}{\partial y \partial x} + (\frac{1-2\nu}{2})(\frac{\partial^2 u_x}{\partial y^2} + \frac{\partial^2 u_y}{\partial x \partial y}) = 0 \\ 
2(1-\nu)\frac{\partial^2 u_x}{\partial x^2} + \frac{\partial^2 u_y}{\partial y \partial x} + (1-2\nu)\frac{\partial^2 u_x}{\partial y^2}  = 0. 
\end{gathered}
\label{eq:eqfirst}
\end{equation}
An expression for the second component can be determined similarly 
\begin{equation}
\begin{gathered}
\nu  \frac{\partial^2 u_x}{\partial x \partial y} + (1-\nu) \frac{\partial^2 u_y}{\partial y^2} + (\frac{1-2\nu}{2})(\frac{\partial^2 u_x}{\partial y \partial x} + \frac{\partial^2 u_y}{\partial x^2}) = 0\\
2(1-\nu) \frac{\partial^2 u_y}{\partial y^2} + \frac{\partial^2 u_x}{\partial x \partial y} + (1-2\nu)\frac{\partial^2 u_y}{\partial x^2} = 0. 
\end{gathered}
\label{eq:eqsecond}
\end{equation}
Evaluating the partial derivatives in the first component, equation \ref{eq:eqfirst}, yields
\begin{equation}
\frac{\partial^2 u_x}{\partial x^2} = \frac{\partial^2 u_x}{\partial y^2} = \frac{\partial^2 u_y}{\partial x \partial y} = 0.
\end{equation}
Hence, the equilibrium equation for the first component, equation \ref{eq:eqfirst}, is satisfied. Evaluating the partial derivatives in the second component, equation \ref{eq:eqsecond}, yields
\begin{equation}
\begin{gathered}
\frac{\partial^2 u_y}{\partial y^2} = 2\frac{f_{max}\nu(1+\nu)}{El}\\
\frac{\partial^2 u_x}{\partial y \partial x} = -2\frac{f_{max}(1-\nu^2)}{El} \\ 
\frac{\partial^2 u_y}{\partial x^2} = 2\frac{f_{max}(1-\nu^2)}{El}.
\end{gathered}
\end{equation}
Substituting these into the equation \ref{eq:eqsecond} yields
\begin{equation}
\begin{gathered}
\frac{f_{max}}{El}\left(4\nu(1-\nu^2) -2(1-\nu^2) + 2(1-2\nu)(1-\nu^2)\right) = \frac{f_{max}}{El} (1-\nu^2)(4\nu - 2 + 2-4\nu) = 0.
\end{gathered}
\end{equation}
Hence, the equilibrium equation for the second component, equation \ref{eq:eqsecond}, is satisfied.

\pagebreak
\section*{Question 2}
The strong form of the displacement problem is to solve the governing equations
\begin{equation}
\begin{gathered}
\nabla_s^T \bm{\sigma} + \bm{b} = \bm{0}, \\
\bm{\sigma} = \bm{D} \bm{\epsilon}, \\
\bm{\epsilon} = \nabla_s \bm{u},
\end{gathered}
\label{eq:governing}
\end{equation}
subject to the traction boundary conditions
\begin{equation}
\begin{gathered}
\bm{\overline{t}} = \bm{\overline{t}}\big|_R = \bm{f} \qquad x=L, 0 \leq  y \leq l, \\
\bm{\overline{t}} = \bm{\overline{t}}\big|_T = \bm{0} \qquad 0 \leq x \leq L, y = l, \\ 
\bm{\overline{t}} = \bm{\overline{t}}\big|_B = \bm{0} \qquad 0 \leq x \leq L, y = 0, \\
\overline{t}_y = \overline{t}_y\big|_L = 0 \qquad x = 0, 0 \leq y \leq l \\
\end{gathered}
\label{eq:trac boundary}
\end{equation}
and displacement boundary conditions
\begin{equation}
\begin{gathered}
\overline{u_x} = 0, \qquad x = 0, 0 \leq y \leq l, \\
\overline{u_y} = 0, \qquad x = y = 0.
\end{gathered}
\label{eq:disp boundary}
\end{equation}
However, since there are no body forces, $\bm{b}$ need not be considered. To derive the weak for the equilibrium equation is first multiplied by an arbitrary weighting factor, $\bm{w}$, of the same form as $\bm{u}$ and integrated over the domain
\begin{equation}
\begin{gathered}
\int_\Omega \bm{w}^T \nabla_s^T \bm{\sigma} d \Omega = 0 \qquad \forall \bm{w}
\end{gathered}
\end{equation}
Although it will not explicitly written, $\bm{w}$ will remain arbitrary throughout the following working (i.e. $\forall \bm{w}$ applies continuously). Applying Green's Theorem yields
\begin{equation}
\begin{gathered}
- \int_\Omega (\nabla_s\bm{w})^T  \bm{\sigma} d \Omega  + \int_\Gamma \bm{w}^T  \bm{\tau n} d \Gamma = 0.
\end{gathered}
\label{eq:greens}
\end{equation}
We then set $\bm{w=0}$ on $\Gamma_D$ and use the fact that $\bm{\tau n} = \bm{\overline{t}}$ on $\Gamma_N$ to equation \ref{eq:greens} as 
\begin{equation}
\begin{gathered}
\int_\Omega (\nabla_s\bm{w})^T  \bm{\sigma} d \Omega  = \int_{\Gamma _N} \bm{w}^T \bm{\overline{t}} d \Gamma
\end{gathered}
\label{eq:int weak form}
\end{equation}

\subsection*{a) Weak form in terms of displacements expressed using the Lam\'e constants $\lambda$ and $\mu$}
An expression for $\bm{\sigma}$ can be found in terms of $\bm{u}$ as follows
\begin{equation}
\bm{\sigma} = \bm{D\epsilon} = \bm{D}\nabla_s^T\bm{u}.
\end{equation}
Equation \ref{eq:int weak form} can then be written as
\begin{equation}
\int_\Omega (\nabla_s\bm{w})^T  \bm{D}\nabla_s^T\bm{u}d \Omega  = \int_{\Gamma _N} \bm{w}^T \bm{\overline{t}} d \Gamma.
\end{equation}
The elasticity matrix, $\bm{D}$, can be decomposed into the summation of two matrices dependent on the summation of the Lam\'e constants $\lambda$ and $\mu$ as 
\begin{equation}
\bm{D} = \bm{D}_\lambda + \bm{D}_\mu = \lambda \left[\begin{array}{c c c}
1 & 1 & 0 \\
1 & 1 & 0\\
0 & 0 & 0
\end{array}\right] + \mu\left[\begin{array}{c c c}
2 & 0 & 0 \\
0 & 2 & 0\\
0 & 0 & 1
\end{array}\right]
\end{equation} 
Hence, the weak form can be further expanded to have two different integrals that will later contribute to the stiffness matrix
\begin{equation}
\int_\Omega (\nabla_s\bm{w})^T  \bm{D_\lambda}\nabla_s^T\bm{u}d \Omega  + \int_\Omega (\nabla_s\bm{w})^T  \bm{D_\mu}\nabla_s^T\bm{u}d \Omega   = \int_{\Gamma _N} \bm{w}^T \bm{\overline{t}} d \Gamma.
\label{eq:disp wf}
\end{equation}
Hence the weak form is: find $\bm{u}\in [u]^2$ that satisfies equation \ref{eq:disp wf} for all $\bm{w}\in [u_0]^2$, where 
\begin{equation}
\begin{gathered}
u = \left\{v\big|v \in H^1, \overline{v} = \overline{u} \quad on \quad \Gamma_D  \right\},\\
u_0 = \left\{v\big|v \in H^1, \overline{v} = 0 \quad on \quad \Gamma_D  \right\}
\end{gathered}
\label{eq:spaces}
\end{equation}
Noting the following Galerkin approximations 
\begin{equation}
\begin{gathered}
\bm{u} \approx \bm{Nd},\\
\bm{w} \approx \bm{Nc},\\
\nabla_s \bm{u} \approx \bm{Bd},\\
\nabla_s \bm{w} \approx \bm{Bc}.
\end{gathered}
\label{eq:disp GAs}
\end{equation}
The matrix form of the problem can be found by substituting these approximations in
\begin{equation}
\begin{gathered}
\bm{c^T} \int_\Omega \bm{B}^T  \bm{D_\lambda}\bm{B}d \Omega \bm{d} + \bm{c^T} \int_\Omega \bm{B}^T \bm{D_\mu} \bm{B}d \Omega \bm{d} =  \bm{c}^T\int_{\Gamma _N} \bm{N}^T \bm{\overline{t}} d \Gamma\\
\bm{c^T} \left(\int_\Omega \bm{B}^T  \bm{D_\lambda}\bm{B}d \Omega + \int_\Omega \bm{B}^T \bm{D_\mu} \bm{B}d \Omega \right) \bm{d} =  \bm{c}^T\int_{\Gamma _N} \bm{N}^T \bm{\overline{t}} d \Gamma \\ 
\bm{c^T} \left( \bm{K_\lambda} + \bm{K_\mu} \right) \bm{d} =  \bm{c}^T\bm{F}\\
\Rightarrow \bm{c^T} \left( \left( \bm{K_\lambda} + \bm{K_\mu} \right) \bm{d} - \bm{F} \right) = \bm{0}
\end{gathered}
\end{equation}
Hence, because $\bm{c}^T$ is arbitrary,
\begin{equation}
\left( \bm{K_\lambda} + \bm{K_\mu} \right) \bm{d} = \bm{F}.
\end{equation}

\subsection*{b) Weak form in terms of displacement and pressure}
When deriving the weak form in terns of displacement and pressure it is useful to rewrite $\bm{\sigma}$ in terms of a hydrostatic pressure component $p$ and $\bm{\epsilon}$
\begin{equation}
\bm{\sigma} = -p \left[\begin{array}{c}
1\\ 1 \\ 0
\end{array} \right] +\mu \left[\begin{array}{c c c}
2 & 0 & 0\\
0 & 2 & 0 \\
0 & 0 & 1
\end{array}\right] \bm{\epsilon} = - p \bm{1} + \bm{D_\mu} \ \bm{\epsilon} = - p \bm{1} + \bm{D_\mu} \nabla_s\bm{u}
\end{equation}
Substituting this expression for $\bm{\sigma}$ into equation \ref{eq:int weak form} yields
\begin{equation}
\begin{gathered}
\int_\Omega (\nabla_s\bm{w})^T  \left( - p \bm{1} + \bm{D_\mu} \nabla_s\bm{u}\right) d \Omega  = \int_{\Gamma _N} \bm{w}^T \bm{\overline{t}} d \Gamma,\\
\int_\Omega (\nabla_s\bm{w})^T  \bm{D_\mu} \nabla_s\bm{u}d \Omega  -\int_\Omega p \nabla \cdot \bm{w} d\Omega = \int_{\Gamma _N} \bm{w}^T \bm{\overline{t}} d \Gamma.
\end{gathered}
\label{eq:disp pd wf}
\end{equation}
It is also necessary to add another governing equation relating $p$ to the volume change, $\nabla \cdot \bm{u}$, to the $p$ and via the Lam\'e constant $\lambda$
\begin{equation}
\nabla \cdot \bm{u} = -p/\lambda.
\end{equation}
The weak form for this equation is developed similarly to above. The equation is multiplied by an arbitrary weighting factor $q$ of the same form of $p$ and integrated over the domain
\begin{equation}
\begin{gathered}
\int_\Omega \nabla \cdot \bm{u} d\Omega  = -\int_\Omega q p/\lambda d\Omega \\
-\int_\Omega \nabla \cdot \bm{u} d\Omega -\int_\Omega q p/\lambda d\Omega = 0
\end{gathered}
\label{eq:press pd wf}
\end{equation}
Hence the weak form in terms of displacement and pressure is: Find $\bm{u} \in [u]^2$ and $p \in L^2$ that satisfy equations \ref{eq:disp pd wf} and \ref{eq:press pd wf}, for all $\bm{w} \in [u_0]^2$ and $q \in L^2$, where $u$ and $u_0$ are defined in equation \ref{eq:spaces}. To obtain the matrix form of the problem the same Galerkin approximations displayed in equation \ref{eq:disp GAs} are use along with the following additional approximations
\begin{equation}
\begin{gathered}
\nabla \cdot \bm{u} \approx \bm{Ld}\\
\nabla \cdot \bm{w} \approx \bm{Lc} = \bm{c^TL^T}\\
p \approx \bm{\tilde{N}p} = \bm{p^T\tilde{N}^T}\\
q \approx \bm{\tilde{N}q} = \bm{q^T\tilde{N}^T}
\end{gathered}
\end{equation}
Substituting these approximations into equation \ref{eq:disp pd wf} yields
\begin{equation}
\begin{gathered}
\bm{c^T}\int_\Omega \bm{B^T} \bm{D_\mu} \bm{B}d\Omega \bm{d} - \bm{c^T}\int_\Omega \bm{L^T\tilde{N}} d\Omega \bm{p} = \bm{c^T}\int_{\Gamma_N} \bm{N^T\overline{t}} d \Gamma,\\
\bm{c^T}\bm{\overline{K}d} + \bm{c^T}\bm{Gp} = \bm{c^T}\bm{\overline{F}}, \\
\bm{c^T}\left(\bm{\overline{K}d} + \bm{Gp} - \bm{\overline{F}}\right) = \bm{0}.
\end{gathered}
\end{equation}
Since $\bm{c}^T$ is arbitrary
\begin{equation}
\bm{\overline{K}d} + \bm{Gp} = \bm{\overline{F}}.
\label{eq:kf dp mf}
\end{equation}
Similarly for equation equation \ref{eq:press pd wf}
\begin{equation}
\begin{gathered}
-\bm{q^T} \int_\Omega \bm{\tilde{N}^TL}d\Omega \bm{d} -\bm{q^T} \int_\Omega \frac{1}{\lambda}\bm{\tilde{N}^T\tilde{N}}d\Omega \bm{p} = 0,\\
\bm{q^T} \bm{G^Td} + \bm{q^T} \bm{Mp} = \bm{0}, \\
\bm{q^T} \left(\bm{G^Td} +  \bm{Mp} \right) = \bm{0} 
\end{gathered}
\end{equation}
and because $\bm{q^T}$ is arbitrary, it follows that
\begin{equation}
\bm{G^Td} +  \bm{Mp} = \bm{0}.
\label{eq:gm dp mf}
\end{equation}
Hence solving equations \ref{eq:kf dp mf} and \ref{eq:gm dp mf} will lead to the solving of the FEM problem.


\pagebreak
\section*{Question 3}
\begin{figure}[H]
 \centering
\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight ]{v04quad} 
\label{fig:Disp Q04}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight]{v045quad}
\label{fig:Disp Q045}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight]{v0499quad}
\caption{}
\label{fig:Disp Q0499}
\end{subfigure}

\caption{Plots of the exact solution and solutions produced by Q1 elements, Q1 elements with Selective Reduced Integration (SRI) and Q2 elements for Poison's ratios of a) 0.4 b) 0.45 and c) 0.499}
\label{fig:Disp Q}
\end{figure}

\pagebreak
\section*{Question 4}
\begin{figure}[H]
 \centering
\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight ]{tri04} 
\label{fig:tri04}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight]{tri045}
\label{fig:tri045}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth, height =0.2\paperheight]{tri0499}
\caption{}
\label{fig:tri0499}
\end{subfigure}

\caption{Plots of the exact solution and solutions produced by P1 elements for Poison's ratios of a) 0.4 b) 0.45 and c) 0.499}
\label{fig:tri}
\end{figure}

\pagebreak
\section*{Question 5}
\begin{figure}[H]
 \centering
\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth, height =0.16 \paperheight]{v04pd} 
\caption{ }
\label{fig:pd04}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth, height =0.16 \paperheight]{v045pd}
\caption{ }
\label{fig:pd045}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth, height =0.16 \paperheight]{v0499pd}
\caption{ }
\label{fig:pd0499}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth, height =0.16 \paperheight]{v05pd}
\caption{ }
\label{fig:pd05}
\end{subfigure}

\caption{Plots of the exact solution and solutions produced by P1-P0 elements, Q1-P0 elements and Q2-P1 elements for Poison's ratios of a) 0.4 b) 0.45, c) 0.499 d) 0.5}
\label{fig:pd}
\end{figure}


\pagebreak
\section*{Question 6}
To apply pressure smoothing, the vector $\bm{\tilde{P}}$ must be solved for in the matrix system
\begin{equation}
\bm{Y\tilde{P} = P}.
\end{equation}
Where $\bm{Y}$ and $\bm{P}$ are defined by 
\begin{equation}
\begin{gathered}
\bm{Y} = \sum_{e=1}^{n_{el}} \int_{\Omega^e} \bm{{N^e}^T} \bm{N^e} d\Omega, \\
\bm{P} = \sum_{e=1}^{n_{el}} p_m^e \int_{\Omega^e} \bm{{N^e}^T} d\Omega.
\end{gathered}
\end{equation}
Where $p_m^e$ is the average pressure over the element and $\bm{N^e}$ is the scalar shape function array for the element. Note that in this case the summation is an assembly summation. In the case of a P0 pressure element, $p_m^e$ will be the value of the pressure over the entire element since it is constant. In the case of a Q1 displacement element 
\begin{equation}
\bm{N^e} = \frac{1}{4} \left[\begin{array}{c c c c}
(1 - \eta)(1 - \xi) & (1 - \eta)(1 + \xi) & (1 + \eta)(1 + \xi)  & (1 + \eta)(1 - \xi) 
\end{array} \right].
\end{equation}
Once the matrix system is solved, the smoothed pressure value at a point in an element, $\tilde{p}^e$, can be found using the equation
\begin{equation}
\tilde{p}^e = \bm{N^e \tilde{P}^{e}}.
\end{equation}

\pagebreak
\section*{Question 7}
\begin{figure}[H]
 \centering
\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{discont04} 
\label{fig:discont04}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{discont045}
\label{fig:discont045}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{discont0499}
\caption{}
\label{fig:discont0499}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{discont05}
\caption{}
\label{fig:discont05}
\end{subfigure}

\caption{Plots of discontinuous pressure field produced by Q1-P0 elements for Poison's ratios of a) 0.4 b) 0.45 c) 0.499 and d) 0.5}
\label{fig:discont}
\end{figure}


\begin{figure}[H]
 \centering
\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{smoot04} 
\label{fig:smooth04}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{smoot045}
\label{fig:smooth045}
\caption{ }
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{smoot0499}
\caption{}
\label{fig:smooth0499}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=\linewidth]{smoot05}
\caption{}
\label{fig:smooth05}
\end{subfigure}

\caption{Plots of continuous pressure field produced by pressure smoothing of Q1-P0 elements for Poison's ratios of a) 0.4 b) 0.45 c) 0.499 and d) 0.5}
\label{fig:smooth}
\end{figure}
\pagebreak



\section*{Discussion}
\subsection*{Vertical tip displacement}
The numerical values of the tip displacement (i.e at point $(L,l)$) can be seen in Table \ref{tab:d}.
\begin{table}[h!]
\centering
\caption{Vertical displacement at point $(L, l)$ for different solutions}
\begin{tabular}{c | c c c c} 
 \hline
Element & $\nu=0.4$ & $\nu=0.45$ & $\nu=0.499$ & $\nu=0.5$    \\ 
 \hline\hline
 Exact solution & 84.00 & 79.75 & 75.10 & 75.00 \\ 
 Q2 & 84.00 & 79.75 & 75.10 & - \\
 Q2-P1 & 84.00 & 79.75 & 75.10 & 75.00 \\ 
 Q1 & 50.33 & 41.19 & 2.31 & - \\
 Q1 SRI & 61.37 & 60.55 & 59.29 & - \\ 
 Q1-P0 & 61.36 & 60.55 & 59.29 & 59.26 \\ 
 P1 & 26.37 & 22.98 & 12.87 & - \\ 
 P1-P0 & 26.37 & 22.98 & 12.87 & 12.40\\  
 \hline
\end{tabular}
\label{tab:d}
\end{table}
The table shows that both the Q2 and the Q2-P1 solutions consistently match the exact solution. This is to be expected since Q2 elements are biquadratic, hence they should be able to model biquadratic solutions (like the exact solution to this problem) exactly. Although the Q2 and the Q2-P1 elements produce the same results for compressible cases, the Q2-P1 element has an advantage over the Q2 element in that it can model the fully incompressible case, whereas the Q2 element cannot.

The Q1 element produces `reasonable' results for the lower values of Poison's ratio using a coarse mesh. However, it displays severe locking at the near incompressible case of $\nu=0.499$; this can be seen both in table \ref{tab:d} and figure \ref{fig:Disp Q0499}. Table \ref{tab:d} shows that the Q1 element with Selective Reduced Integration (SRI) consistently out performs the standard Q1 element and, as shown in figure \ref{fig:Disp Q0499}, does not display locking characteristics at the near incompressible case. The Q1-P0 element gives results that exactly replicate the Q1 SRI element in the compressible case. This is to be expected due to the reduced integration equivalence theorem that states that a Q1 SRI element will behave like a Q1-P0 element for compressible motion \cite{bevlec}. However, the Q1-P0 element can also be used to model the incompressible motion, whereas the Q1 SRI element cannot. All the Q1 elements are out performed by the Q2 elements; this is to be expected as the Q1 elements cannot model quadratic behaviour exactly like Q2 elements can.

The P1 elements produce poor approximations relative to the other elements. Furthermore, using a P1-P0 element does not seem to improve the solutions at all. This may be because the P1-P0 element has a constraint ratio of 1 \cite{hughes2012finite}, and hence will still have a tendency to lock. The Q1-P0 does not have this problem as it has the ideal constraint ratio of 2 \cite{hughes2012finite}. Although the P1-P0 element does not improve the solution of the P1 element in the compressible case, it does allow one to model the incompressible case.

\subsection*{Pressure smoothing}
Both the discontinuous and smoothed pressure distributions are completely antisymmetric about the neutral axis (center of the beam). This is to be expected as the strong form of the problem is antisymmetric. Furthermore, since the stress is constant through the length of the beam, the pressure is also constant through the length of the beam. The upper portion of the beam is experiencing compressive loading and the lower portion of the beam is experiencing tensile loading, hence the upper portion of the beam is experiencing a positive pressure and the lower portion of the beam is experiencing a negative pressure. Since the pressure is smoothed using linear shape functions, the smoothed pressure at the centre of each element is the same value as the discontinuous pressure of that element. The linearity of the shape functions as well as the antisymmetry of the problem gives rise to the smoothed pressure being 0 at the neutral axis and double that of the discontinuous pressure at the edges.
\pagebreak



\printbibliography
\end{document}
