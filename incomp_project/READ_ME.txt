Dear Reader

This READ ME file serves as a general road map to understanding the code written in this project.

The code is written in an Object Orientated since. In the 'modules' directory you will find 4 python packages.
Each python package contains a class/classes.

*** continuous_2D_vector_elements ***
The continuous_2D_vector_elements package contains a class for Q1
elements, P1 elements and Q2 elements. Each class contains information relevant to that particular element that is
defined in the __init__ function in the class (note that I am not referring to the __innit__.py file in which the
classes are found, but the actual __init__ function found at the top of the class deceleration). These bits of information
(such as the number of nodes in the element, an array of the element's displacement values etc) are known as *attributes*.
Each class has a certain number of *methods* can both operate on the attributes of the class (or object) and return certain
values (such as an array of shape function values).

*** discontinuous_2D_vector_elements ***
The discontinuous_2D_scalar_elements package contains a class for P0 elements and a class for P1 elements. Each class
contains the relevant attributes and methods for those elements.

*** mixed_elements ***
The mixed_elements package contains one class called QnPn that takes in an arbitrary continuous_2D_vector_element object and
an arbitrary discontinuous_2D_scalar_elements object and combines them into the relevant pressure displacement mixed element.
It 'maps' the attributes and methods of the relevant objects into one object. It also creates two new methods; one to obtain
the L array and one to determine the smoothed pressure at some xi, eta position in the element.

*** Mesh_2D_Vec ***
The Mesh_2D_Vec package contains three classes: Mesh_2D_Vec_Std, Mesh_2D_Vec_SRI and Mixed_mesh. The Mesh_2D_Vec_Std class
deals only with pure displacement elements, the Mesh_2D_Vec_SRI class deals purely with displacement elements but it allows
for selective reduced integration and the Mixed_mesh deals purely with pressure-displacement elements. The mesh class contain
attributes and methods that are relevant to the whole mesh such as the global stiffness matrix and procedure of applying boundary conditions.
One very key attribute that all the mesh classes have is an array called Elements. This is an array of element objects (an
object is an instantiation of a class). Many methods in the mesh classes will loop through the Elements array and use one
or multiple methods or attributes of each element.

*** Implementation files ***
None of the python packages do anything on their own - they have to be implemented in an implementation file. The implementation
file will define things like the ICA, nodal positions, traction and displacement boundary conditions etc and then feed these
into a mesh class along with the type of element that will be used. This will create a mesh object that can be used to apply
integration to get the force vector, stiffness matrix, solve the problem, plot the solution among other things. Let the implementation
files guide you to try and figure out what is being done. There is a lot of code in the Mesh_2D_Vec class that is left over from
other projects so the whole class is ca. 2000 lines long. Only look for the methods that are being used in the implementation
files since those are the only bits of code that are actually being used. For this project the implementation files have been named
intuitively (question3.py, question4.py, etc) running those scripts will automatically produce the results required for
the corresponding question. The only thing that will need changing in each script is the Poison's ratio being used. This
value is set very near the top of each file and is always called "vtest". So setting Poison's ratio to 0.45 will look
something like "vtest = 0.45".

I hope this helps.

Regards

Benjamin Alheit