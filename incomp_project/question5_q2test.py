import Mesh_2D_Vec
import continuous_2D_vector_elements as ct
import discontinuous_2D_scalar_elements as dt

import numpy as np
import matplotlib.pyplot as plt
import math

L = 10
l = 2
f_max = 3000
vtest = 0.4
Ein = 1500
scale = 0.01


points = 100

xh = np.linspace(0, L, points)
xv = np.zeros(points)
yh = np.zeros(points)
yv = np.linspace(0, l, points)

x = np.append(xh, xv + L)
x = np.append(x, np.flip(xh, axis=0))
x = np.append(x, xv)

y = np.append(yh, yv)
y = np.append(y, yh + l)
y = np.append(y, np.flip(yh, axis=0))

def get_ux(x,y):
    return (2 * f_max * (1-(vtest**2)) / (Ein * l)) * np.multiply(x, (l/2 - y))

def get_uy(x, y):
    return (f_max * (1-(vtest**2)) / (Ein * l)) * (np.power(x, 2) + vtest/(1-vtest) * np.multiply(y, y-l))

ux = get_ux(x, y)
uy = get_uy(x, y)

def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return vtest

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 1500


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs(x - L) < 0.000000000001:
        return np.array([(2*f_max/l)*(l/2 - y), 0])
    else:
        return np.array([0, 0])



def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


edges = 2
n = edges * 2
row_nodes = n + 1
x_verticies = np.array([0, L, L, 0])    # The x positions of the nodes
y_verticies = np.array([0, 0, l, l])    # The y positions of the nodes
# nodes_x = np.insert(np.linspace(0, 1, n), np.arange(0, n), np.linspace(0, 1, n))    # The x positions of the nodes
# nodes_y = np.array([0, 0.1] * n)    # The y positions of the nodes
nodes = (n+1) ** 2
els = int(0.9+(n ** 2)/4)
x_12 = x_verticies[1] - x_verticies[0]
y_12 = y_verticies[1] - y_verticies[0]
y_23 = y_verticies[2] - y_verticies[1]
y_14 = y_verticies[3] - y_verticies[0]

nodes_x = np.zeros(nodes)
nodes_y = np.zeros(nodes)
# nodes_x = np.array([0, 48, 48, 0])
# nodes_y = np.array([0, 44, 60, 44])

BC = np.zeros(nodes*2)
# BC[[0, 1, 6, 7]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
c = 0

for i in range(n + 1):
    y_start = i * y_14/n
    y_end = y_verticies[1] + i * y_23/n
    y_diff = y_end - y_start
    for j in range(n + 1):
        nodes_x[c] = j*x_12/n
        nodes_y[c] = y_start + j * y_diff / n
        if nodes_x[c] == 0:
            BC[[2*c, 2*c+1]] = 1
        c += 1


ICA = np.zeros([els, 9], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

for i in range(els):
    p = int((row_nodes-1)/2)
    p3 =i%int((row_nodes-1)/2)
    p1 = 2*(i%int((row_nodes-1)/2))
    p2 = math.floor(2*(i+1)/row_nodes)
    p4 = math.floor(i/edges)
    bl = 2*(i%int((row_nodes-1)/2)) + 1 + math.floor(i/edges)*2*row_nodes
    # ICA[i, :] = np.array([bl, bl+1, bl + n + 2, bl + n + 1])
    ICA[i, :] = [int(bl), int(bl+2), int(bl + 2*row_nodes + 2), int(bl + 2*row_nodes), int(bl+1), int(bl+2+row_nodes), int(bl+1+2*row_nodes), int(bl+row_nodes), int(bl+1+row_nodes)]

PCA = np.arange(1, 1 + els*3).reshape([els, 3])

# PCA = np.arange(1, 1 + els).reshape([els, 1])

#
# PCA = np.array([[1, 2, 3],
#                 [4, 5, 6],
#                 [7, 8, 9],
#                 [10, 11, 12],
#                 [13, 14, 15],
#                 [16, 17, 18],
#                 [19, 20, 21],
#                 [22, 23, 24]
#                 ])

# Creating a triangle mesh object with given set up
quad_mesh = Mesh_2D_Vec.Mixed_Mesh(nodes_x,
                                    nodes_y,
                                    ICA,
                                    E,
                                    v,
                                    t_bar,
                                    u_bar,
                                    b,
                                    BC,
                                    "strain",
                                    thick,
                                   PCA,
                                   ct.Q2,
                                   dt.P1)


quad_mesh.int_F_bound(3)
quad_mesh.int_F_bod(3)  # Performing integration to create body force vectors

quad_mesh.int_K_bar(3)
quad_mesh.int_G(3)
quad_mesh.int_M(3)

quad_mesh.assemble()  # Assembling element matrices and vectors into global
# quad_mesh.F_big /= 2
quad_mesh.solve_d()  # Solving for global d
#
#
# quad_mesh.plot_displacement("mixed test",
#                             scale=scale,
#                             lines_per_edge=4,
#                             show_nodes_numbers=False,
#                             show_nodes=True,
#                             show_element_numbers=False)

plot = plt

plot.plot(x + scale * ux, y + scale*uy, linestyle="--", linewidth=4, color='grey', label='Analytical solution')

plot.grid()
plot.xlabel('x (m)')
plot.ylabel('y (m)')
plot.title('Pressure-displacement element solutions for a Poison\'s ratio of ' + str(vtest) + '\nUsing a scale factor of ' +str(scale) + ' for the displacement')

plot = quad_mesh.add_to_plot(plot,
                      scale=scale,
                      color='black',
                      line_style='--',
                      marker='s',
                      legend='P1-P0',
                      lines_per_edge=10,
                      show_nodes=True,
                      show_element_numbers=False,
                      show_nodes_numbers=False)


plot.legend()
plot.show()