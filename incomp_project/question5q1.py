import Mesh_2D_Vec
import continuous_2D_vector_elements as ct
import discontinuous_2D_scalar_elements as dt

import numpy as np
import matplotlib.pyplot as plt
import math

L = 10
l = 2
f_max = 3000
vtest = 0.5
Ein = 1500
scale = 0.01


points = 100

xh = np.linspace(0, L, points)
xv = np.zeros(points)
yh = np.zeros(points)
yv = np.linspace(0, l, points)

x = np.append(xh, xv + L)
x = np.append(x, np.flip(xh, axis=0))
x = np.append(x, xv)

y = np.append(yh, yv)
y = np.append(y, yh + l)
y = np.append(y, np.flip(yh, axis=0))

def get_ux(x,y):
    return (2 * f_max * (1-(vtest**2)) / (Ein * l)) * np.multiply(x, (l/2 - y))

def get_uy(x, y):
    return (f_max * (1-(vtest**2)) / (Ein * l)) * (np.power(x, 2) + vtest/(1-vtest) * np.multiply(y, y-l))

ux = get_ux(x, y)
uy = get_uy(x, y)

def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return vtest


def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 1500


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs(x - L) < 0.000000000001:
        return np.array([(2 * f_max / l) * (l / 2 - y), 0])
    else:
        return np.array([0, 0])


def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


nodes_x = np.array([0, 0, 0, L / 4, L / 4, L / 4, L / 2, L / 2, L / 2, 3 * L / 4, 3 * L / 4, 3 * L / 4, L, L, L])
nodes_y = np.array([0, l / 2, l, 0, l / 2, l, 0, l / 2, l, 0, l / 2, l, 0, l / 2, l])
# nodes_x = np.array([0, 48, 48, 0])
# nodes_y = np.array([0, 44, 60, 44])

BC = np.zeros(2 * np.alen(nodes_x))
BC[[0, 1, 2, 4]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

# ICA = np.zeros([8, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

ICA = np.array([[1, 4, 5, 2],
                [2, 5, 6, 3],
                [4, 7, 8, 5],
                [5, 8, 9, 6],
                [7, 10, 11, 8],
                [8, 11, 12, 9],
                [10, 13, 14, 11],
                [11, 14, 15, 12]])
els = 8

PCA = np.arange(1, 1 + els*3).reshape([els, 3])
#
# PCA = np.array([[1],
#                 [2],
#                 [3],
#                 [4],
#                 [5],
#                 [6],
#                 [7],
#                 [8]
#                 ])

# Creating a triangle mesh object with given set up
quad_mesh = Mesh_2D_Vec.Mixed_Mesh(nodes_x,
                                    nodes_y,
                                    ICA,
                                    E,
                                    v,
                                    t_bar,
                                    u_bar,
                                    b,
                                    BC,
                                    "strain",
                                    thick,
                                   PCA,
                                   ct.Q1,
                                   dt.P0)


quad_mesh.int_F_bound(3)
quad_mesh.int_F_bod(3)  # Performing integration to create body force vectors

quad_mesh.int_K_bar(3)
quad_mesh.int_G(3)
quad_mesh.int_M(3)

quad_mesh.assemble()  # Assembling element matrices and vectors into global
quad_mesh.solve_d()  # Solving for global d

# quad_mesh.plot_displacement("mixed test",
#                             scale=scale,
#                             lines_per_edge=1,
#                             show_nodes_numbers=False,
#                             show_nodes=True,
#                             show_element_numbers=False)

plot = plt

plot.plot(x + scale * ux, y + scale*uy, linestyle="--", linewidth=4, color='grey', label='Analytical solution')

plot.grid()
plot.xlabel('x (m)')
plot.ylabel('y (m)')
plot.title('Pressure-displacement element solutions for a Poison\'s ratio of ' + str(vtest) + '\nUsing a scale factor of ' +str(scale) + ' for the displacement')

plot = quad_mesh.add_to_plot(plot,
                      scale=scale,
                      color='black',
                      line_style='--',
                      marker='s',
                      legend='P1-P0',
                      lines_per_edge=1,
                      show_nodes=True,
                      show_element_numbers=False,
                      show_nodes_numbers=False)


plot.legend()
plot.show()