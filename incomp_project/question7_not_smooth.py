import sys
try:
    sys.path.append('../modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements as ct
    import discontinuous_2D_scalar_elements as dt
except:
    try:
        sys.path.append('./modules')
        import Mesh_2D_Vec
        import continuous_2D_vector_elements as ct
        import discontinuous_2D_scalar_elements as dt

    except:
        print("Incorrect project structure: Can't find modules")

import numpy as np
import matplotlib.pyplot as plt
import math

L = 10
l = 2
f_max = 3000
vtest = 0.5
Ein = 1500
scale = 0.01


def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return vtest


def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 1500


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs(x - L) < 0.000000000001:
        return np.array([(2 * f_max / l) * (l / 2 - y), 0])
    else:
        return np.array([0, 0])


def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


nodes_x = np.array([0, 0, 0, L / 4, L / 4, L / 4, L / 2, L / 2, L / 2, 3 * L / 4, 3 * L / 4, 3 * L / 4, L, L, L])
nodes_y = np.array([0, l / 2, l, 0, l / 2, l, 0, l / 2, l, 0, l / 2, l, 0, l / 2, l])
# nodes_x = np.array([0, 48, 48, 0])
# nodes_y = np.array([0, 44, 60, 44])

BC = np.zeros(2 * np.alen(nodes_x))
BC[[0, 1, 2, 4]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

# ICA = np.zeros([8, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

ICA = np.array([[1, 4, 5, 2],
                [2, 5, 6, 3],
                [4, 7, 8, 5],
                [5, 8, 9, 6],
                [7, 10, 11, 8],
                [8, 11, 12, 9],
                [10, 13, 14, 11],
                [11, 14, 15, 12]])

PCA = np.array([[1],
                [2],
                [3],
                [4],
                [5],
                [6],
                [7],
                [8]
                ])

# Creating a triangle mesh object with given set up
quad_mesh = Mesh_2D_Vec.Mixed_Mesh(nodes_x,
                                    nodes_y,
                                    ICA,
                                    E,
                                    v,
                                    t_bar,
                                    u_bar,
                                    b,
                                    BC,
                                    "strain",
                                    thick,
                                   PCA,
                                   ct.Q1,
                                   dt.P0)


quad_mesh.int_F_bound(3)
quad_mesh.int_F_bod(3)  # Performing integration to create body force vectors

quad_mesh.int_K_bar(3)
quad_mesh.int_G(3)
quad_mesh.int_M(3)

quad_mesh.assemble()  # Assembling element matrices and vectors into global
quad_mesh.solve_d()  # Solving for global d


quad_mesh.plot_discontinuous_pressure("Discontinuous pressure plot of Q1-P0 element with $\\nu$ = " + str(vtest) + "\nDisplacement scaled by " +str(scale),
                                      "x(m)",
                                      'y (m)',
                                      32,
                                      scale,
                                      1,
                                      'o',
                                      'black',
                                      'Blah',
                                      '-',
                                      True)