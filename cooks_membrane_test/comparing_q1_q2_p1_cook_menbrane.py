import edge_based_function_file as ff
import matplotlib.pyplot as plt
import numpy as np
import time

tests = 5
edges = np.array([2, 4, 8, 16, 32])
p1_disp = np.empty(tests, dtype=float)
q1_disp = np.empty(tests, dtype=float)
p1_SRI_disp = np.empty(tests, dtype=float)
q1_SRI_disp = np.empty(tests, dtype=float)
q2_disp = np.empty(tests, dtype=float)


def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    # return 0.3
    return 0.49995

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 250


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs(x - 48)/48 < 0.000000000001:
        return np.array([0, 100/16])
    else:
        return np.array([0, 0])


def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])

t_start = time.time()
for i in range(tests):
    elapsed = str(time.time() - t_start)
    print("loop: ", i + 1, "/", tests, "\ntime elapsed: ", elapsed[:elapsed.find(".")+2])
    print("P1...")
    sim_mesh = ff.P1(edges[i], thick, E, v, b, t_bar, u_bar)
    p1_disp[i] = np.max(sim_mesh.d)
    print("P1_SRI...")
    sim_mesh = ff.P1_SRI(edges[i], thick, E, v, b, t_bar, u_bar)
    p1_SRI_disp[i] = np.max(sim_mesh.d)
    print("Q1...")
    sim_mesh = ff.Q1(edges[i], thick, E, v, b, t_bar, u_bar)
    q1_disp[i] = np.max(sim_mesh.d)
    print("Q1_SRI...")
    sim_mesh = ff.Q1_SRI(edges[i], thick, E, v, b, t_bar, u_bar)
    q1_SRI_disp[i] = np.max(sim_mesh.d)
    print("Q2...")
    sim_mesh = ff.Q2(edges[i], thick, E, v, b, t_bar, u_bar)
    q2_disp[i] = np.max(sim_mesh.d)
    print("")

elapsed = str(time.time() - t_start)
print("Total time: ", elapsed[:elapsed.find(".")+2])

plt.plot(edges, p1_disp, label="P1", marker='s')
plt.plot(edges, q1_disp, label="Q1", marker='o')
# plt.plot(edges, p1_SRI_disp, label="P1_SRI", marker='s')
plt.plot(edges, q1_SRI_disp, label="Q1_SRI", marker='o')
plt.plot(edges, q2_disp, label="Q2", marker='v')
plt.xlabel("Elements on bottom edge")
plt.ylabel("Tip displacement")
plt.title("Cook's membrane test results for different elements")
plt.legend()
plt.grid()
plt.show()