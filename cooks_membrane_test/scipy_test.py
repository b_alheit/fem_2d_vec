import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splg
a = np.array([[1, 0, 3],
              [2, 3, 0],
              [4, 0, 5]])

b = sp.csr_matrix(a)
c = np.array([1, 2, 3])
print(b)
print("a inv")
print(np.matmul(np.linalg.inv(a), c))
print("b inv")
print(splg.spsolve(b, c))