try:
    import sys
    sys.path.append(sys.path[0][:sys.path[0].find('fem_2d_vec')] + 'fem_2d_vec/modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements
except:
    print("Modules may be imported incorrectly due to invalid directory structure.\n"
          "Structure should be: ./fem_2d_vec/.../<this script>.py\n"
          "                     ./fem_2d_vec/modules")

import numpy as np


def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return 0.28

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 15000


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    # return np.array([0, - 2e5 * thick(x, y)])
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    # return np.array([0, -50])
    if abs((y + x - 3)/(abs(y) + abs(x))) < 0.0000001:
        return (1/(2**0.5)) * np.array([15, 15])
    else:
        return np.array([0, 0])



def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0 or y == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


nodes_x = np.array([0, 0, 0, 1.5, 1.5, 3])    # The x positions of the nodes
nodes_y = np.array([3, 1.5, 0, 1.5, 0, 0])    # The y positions of the nodes

BC = np.zeros(len(nodes_x)*2)
BC[[0, 2, 4, 5, 9, 11]] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes


ICA = np.array([[1, 2, 4],
                [2, 3, 4],
                [3, 5, 4],
                [4, 5, 6]])  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

# Creating a triangle mesh object with given set up
tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x, nodes_y, ICA, E, v, t_bar, u_bar, b, BC, "stress", thick, continuous_2D_vector_elements.P1)

# tri_mesh.display_mesh("big mesh", False, False, False)
tri_mesh.int_F_bound(3)
tri_mesh.int_K(1)   # Performing integration to create stiffness matricies
tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
tri_mesh.assemble()     # Assembling element matrices and vectors into global
tri_mesh.solve_d()      # Solving for global d

print("Deflection : ", tri_mesh.d)    # Displaying node deflections
print("Original node positions: ", tri_mesh.pos)    # Displaying old node positions
print("New node positions: ", tri_mesh.pos + tri_mesh.d)    # Displaying new node positions
for i in range(len(tri_mesh.Elements)):
    e = tri_mesh.Elements[i]
    print("Stress in el "+str(i+1)+": ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e.B(0, 0), e.d))) # Calculating and displaying stress in element 2
# e1 = tri_mesh.Elements[0]   # Creating a reference to element 1
# e2 = tri_mesh.Elements[1]   # Creating a reference to element 2
# print("Stress in el 1: ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e1.B(0, 0), e1.d))) # Calculating and displaying stress in element 1
# print("Stress in el 2: ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e2.B(0, 0), e2.d))) # Calculating and displaying stress in element 2

tri_mesh.plot_displacement("Plane strain beam solution", 100, 1, True, True, True)   # Plotting the old and new node positions
