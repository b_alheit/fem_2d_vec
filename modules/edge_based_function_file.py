try:
    import sys
    sys.path.append(sys.path[0][:sys.path[0].find('fem_2d_vec')] + 'fem_2d_vec/modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements
except:
    print("Modules may be imported incorrectly due to invalid directory structure.\n"
          "Structure should be: ./fem_2d_vec/.../<this script>.py\n"
          "                     ./fem_2d_vec/modules")

import numpy as np
import math


def P1(n, thick, E, v, b, t_bar, u_bar):

    n = int(0.1+n/2)
    
    row_nodes = n+1
    x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
    y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes

    nodes = (n+1) ** 2
    els = 2*(n ** 2)
    x_12 = x_verticies[1] - x_verticies[0]
    y_12 = y_verticies[1] - y_verticies[0]
    y_23 = y_verticies[2] - y_verticies[1]
    y_14 = y_verticies[3] - y_verticies[0]

    nodes_x = np.zeros(nodes)
    nodes_y = np.zeros(nodes)


    BC = np.zeros(nodes*2) # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    c = 0

    for i in range(n + 1):
        y_start = i * y_14/n
        y_end = y_verticies[1] + i * y_23/n
        y_diff = y_end - y_start
        for j in range(n + 1):
            nodes_x[c] = j*x_12/n
            nodes_y[c] = y_start + j * y_diff / n
            if nodes_x[c] == 0:
                BC[[2*c, 2*c+1]] = 1
            c += 1

    ICA = np.zeros([els, 3], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    for i in range(int(els/2)):
        p = i%n
        p1 = math.floor((i+1)/row_nodes)
        bl = i%n + 1 + math.floor(i/n)*row_nodes
        # ICA[i, :] = np.array([bl, bl+1, bl + n + 2, bl + n + 1])
        ICA[i*2, :] = [int(bl), int(bl+1), int(bl + row_nodes)]
        ICA[i*2+1, :] = [int(bl + 1), int(bl + 1 +row_nodes), int(bl +row_nodes)]

    # Creating a triangle mesh object with given set up
    tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                           nodes_y,
                                           ICA,
                                           E,
                                           v,
                                           t_bar,
                                           u_bar,
                                           b,
                                           BC,
                                        "strain",
                                           thick,
                                           continuous_2D_vector_elements.P1)

    # quad_mesh.display_mesh("test")

    tri_mesh.int_F_bound(3)
    tri_mesh.int_K(3)   # Performing integration to create stiffness matricies
    tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    tri_mesh.assemble()     # Assembling element matrices and vectors into global
    tri_mesh.solve_d()      # Solving for global d

    return tri_mesh


def P1_SRI(n, thick, E, v, b, t_bar, u_bar):

    n = int(0.1+n/2)

    row_nodes = n+1
    x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
    y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes

    nodes = (n+1) ** 2
    els = 2*(n ** 2)
    x_12 = x_verticies[1] - x_verticies[0]
    y_12 = y_verticies[1] - y_verticies[0]
    y_23 = y_verticies[2] - y_verticies[1]
    y_14 = y_verticies[3] - y_verticies[0]

    nodes_x = np.zeros(nodes)
    nodes_y = np.zeros(nodes)


    BC = np.zeros(nodes*2) # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    c = 0

    for i in range(n + 1):
        y_start = i * y_14/n
        y_end = y_verticies[1] + i * y_23/n
        y_diff = y_end - y_start
        for j in range(n + 1):
            nodes_x[c] = j*x_12/n
            nodes_y[c] = y_start + j * y_diff / n
            if nodes_x[c] == 0:
                BC[[2*c, 2*c+1]] = 1
            c += 1

    ICA = np.zeros([els, 3], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    for i in range(int(els/2)):
        p = i%n
        p1 = math.floor((i+1)/row_nodes)
        bl = i%n + 1 + math.floor(i/n)*row_nodes
        # ICA[i, :] = np.array([bl, bl+1, bl + n + 2, bl + n + 1])
        ICA[i*2, :] = [int(bl), int(bl+1), int(bl + row_nodes)]
        ICA[i*2+1, :] = [int(bl + 1), int(bl + 1 +row_nodes), int(bl +row_nodes)]

    # Creating a triangle mesh object with given set up
    tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                           nodes_y,
                                           ICA,
                                           E,
                                           v,
                                           t_bar,
                                           u_bar,
                                           b,
                                           BC,
                                           "strain",
                                           thick,
                                           continuous_2D_vector_elements.P1)

    # quad_mesh.display_mesh("test")

    tri_mesh.int_F_bound(3)
    tri_mesh.int_K_SRI(1, 3)   # Performing integration to create stiffness matricies
    tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    tri_mesh.assemble()     # Assembling element matrices and vectors into global
    tri_mesh.solve_d()      # Solving for global d

    return tri_mesh


def Q1(n, thick, E, v, b, t_bar, u_bar):

    x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
    y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes
    nodes = (n+1) ** 2
    els = n ** 2
    x_12 = x_verticies[1] - x_verticies[0]
    y_12 = y_verticies[1] - y_verticies[0]
    y_23 = y_verticies[2] - y_verticies[1]
    y_14 = y_verticies[3] - y_verticies[0]

    nodes_x = np.zeros(nodes)
    nodes_y = np.zeros(nodes)


    BC = np.zeros(nodes*2)  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    c = 0

    for i in range(n + 1):
        y_start = i * y_14/n
        y_end = y_verticies[1] + i * y_23/n
        y_diff = y_end - y_start
        for j in range(n + 1):
            nodes_x[c] = j*x_12/n
            nodes_y[c] = y_start + j * y_diff / n
            if nodes_x[c] == 0:
                BC[[2*c, 2*c+1]] = 1
            c += 1


    ICA = np.zeros([els, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    for i in range(els):
        bl = i + 1 + math.floor(i/n)
        ICA[i, :] = [int(bl), int(bl+1), int(bl + n + 2), int(bl + n + 1)]

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                        "strain",
                                            thick,
                                            continuous_2D_vector_elements.Q1)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K(3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh


def Q1_SRI(n, thick, E, v, b, t_bar, u_bar):

    x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
    y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes
    nodes = (n+1) ** 2
    els = n ** 2
    x_12 = x_verticies[1] - x_verticies[0]
    y_12 = y_verticies[1] - y_verticies[0]
    y_23 = y_verticies[2] - y_verticies[1]
    y_14 = y_verticies[3] - y_verticies[0]

    nodes_x = np.zeros(nodes)
    nodes_y = np.zeros(nodes)


    BC = np.zeros(nodes*2)  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    c = 0

    for i in range(n + 1):
        y_start = i * y_14/n
        y_end = y_verticies[1] + i * y_23/n
        y_diff = y_end - y_start
        for j in range(n + 1):
            nodes_x[c] = j*x_12/n
            nodes_y[c] = y_start + j * y_diff / n
            if nodes_x[c] == 0:
                BC[[2*c, 2*c+1]] = 1
            c += 1


    ICA = np.zeros([els, 4], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    for i in range(els):
        bl = i + 1 + math.floor(i/n)
        ICA[i, :] = [int(bl), int(bl+1), int(bl + n + 2), int(bl + n + 1)]

    # Creating a triangle mesh object with given set up
    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                            "strain",
                                            thick,
                                            continuous_2D_vector_elements.Q1)

    quad_mesh.int_F_bound(3)
    quad_mesh.int_K_SRI(1, 3)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(3)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh


def Q2(edges, thick, E, v, b, t_bar, u_bar):

    n = edges * 2
    row_nodes = n + 1
    x_verticies = np.array([0, 48, 48, 0])    # The x positions of the nodes
    y_verticies = np.array([0, 44, 60, 44])    # The y positions of the nodes

    nodes = (n+1) ** 2
    els = int(0.9+(n ** 2)/4)
    x_12 = x_verticies[1] - x_verticies[0]
    y_12 = y_verticies[1] - y_verticies[0]
    y_23 = y_verticies[2] - y_verticies[1]
    y_14 = y_verticies[3] - y_verticies[0]

    nodes_x = np.zeros(nodes)
    nodes_y = np.zeros(nodes)

    BC = np.zeros(nodes*2)  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes
    c = 0

    for i in range(n + 1):
        y_start = i * y_14/n
        y_end = y_verticies[1] + i * y_23/n
        y_diff = y_end - y_start
        for j in range(n + 1):
            nodes_x[c] = j*x_12/n
            nodes_y[c] = y_start + j * y_diff / n
            if nodes_x[c] == 0:
                BC[[2*c, 2*c+1]] = 1
            c += 1


    ICA = np.zeros([els, 9], dtype=int)  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

    for i in range(els):

        bl = 2*(i%int((row_nodes-1)/2)) + 1 + math.floor(i/edges)*2*row_nodes
        ICA[i, :] = [int(bl), int(bl+2), int(bl + 2*row_nodes + 2), int(bl + 2*row_nodes), int(bl+1), int(bl+2+row_nodes), int(bl+1+2*row_nodes), int(bl+row_nodes), int(bl+1+row_nodes)]


    quad_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x,
                                            nodes_y,
                                            ICA,
                                            E,
                                            v,
                                            t_bar,
                                            u_bar,
                                            b,
                                            BC,
                                        "strain",
                                            thick,
                                            continuous_2D_vector_elements.Q2)


    quad_mesh.int_F_bound(5)
    quad_mesh.int_K(5)   # Performing integration to create stiffness matricies
    quad_mesh.int_F_bod(5)   # Performing integration to create body force vectors
    quad_mesh.assemble()     # Assembling element matrices and vectors into global
    quad_mesh.solve_d()      # Solving for global d

    return quad_mesh

