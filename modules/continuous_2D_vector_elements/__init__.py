import numpy as np

class Q1:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
        """
        Constructor for a 2D vector bilinear element
        :param x_nodes: The x positions of the nodes for the element indexed CCW
        :param y_nodes: The y positions of the nodes for the element indexed CCW (corresponding with the x_nodes indices)
        :param g_node_numbers: The global node numbers (corresponding with the x_nodes indices)
        :param el_num: The number of this element
        """

        self.n_nodes = 4    # The number of nodes that the element has
        self.edges = 4      # The number of edges that the element has
        self.x = x_nodes    # Assigning the x positions of the nodes
        self.y = y_nodes    # Assigning the y positions of the nodes
        self.pos = np.array([x_nodes[0], y_nodes[0],
                             x_nodes[1], y_nodes[1],
                             x_nodes[2], y_nodes[2],
                             x_nodes[3], y_nodes[3]])   # Creating a position array of the
        self.g_nodes = g_node_numbers   # Assigning the global node numbers
        self.g_DOF = (2 * np.array([g_node_numbers[0],
                                   g_node_numbers[0]+0.5,
                                   g_node_numbers[1],
                                   g_node_numbers[1] + 0.5,
                                   g_node_numbers[2],
                                   g_node_numbers[2] + 0.5,
                                   g_node_numbers[3],
                                   g_node_numbers[3] + 0.5,
                                   ])).astype(int)  # Creating an array of degree of freedom numbers
        self.el_num = el_num    # Assigning the element number
        self.el_K = np.zeros([8, 8])    # Initialising an empty element stiffness matrix
        self.el_F = np.zeros(8).T       # Initialising an empty element force vector
        self.el_F_bound = np.zeros(8).T     # Initialising an empty element boundary force vector
        self.el_F_bod = np.zeros(8).T       # Initialising an empty element body force vector
        self.d = np.zeros(8)                # Initialising an empty element displacement vector

    def B(self, xi, eta):
        """
        A method to return the derivative shape function matrix of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The derivative shape function matrix at position (xi, eta)
        """
        B_scalar = np.matmul(np.linalg.inv(self.J_mat(xi, eta)), self.GN(xi, eta))  # Determining the derivative shape
        # function matrix in the scalar matrix format

        # Casting the derivative shape function matrix into the necessary 2D vector matrix format
        return np.array([[B_scalar[0, 0], 0, B_scalar[0, 1], 0, B_scalar[0, 2], 0, B_scalar[0, 3], 0],
                         [0, B_scalar[1, 0], 0, B_scalar[1, 1], 0, B_scalar[1, 2], 0, B_scalar[1, 3]],
                         [B_scalar[1, 0], B_scalar[0, 0], B_scalar[1, 1], B_scalar[0, 1], B_scalar[1, 2], B_scalar[0, 2], B_scalar[1, 3], B_scalar[0, 3]]])

    def N(self, xi, eta):
        """
        A method to return the shape function matrix of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The shape function matrix at position (xi, eta)
        """
        return (1/4)*np.array([[(1 - eta) * (1 - xi), 0, (1 - eta) * (1 + xi), 0, (1 + eta) * (1 + xi), 0, (1 + eta) * (1 - xi), 0],
                               [0, (1 - eta) * (1 - xi), 0, (1 - eta) * (1 + xi), 0, (1 + eta) * (1 + xi), 0, (1 + eta) * (1 - xi)]])
    def N_scalar(self, xi, eta):
        """
        A method to return the shape function matrix of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The shape function matrix at position (xi, eta)
        """
        return (1/4)*np.array([(1 - eta) * (1 - xi), (1 - eta) * (1 + xi), (1 + eta) * (1 + xi), (1 + eta) * (1 - xi)])

    def GN(self, xi, eta):
        """
        A method to return the gradient shape function matrix of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The gradient shape function matrix at position (xi, eta)
        """
        return (1/4)*np.array([[-(1 - eta), (1 - eta), (1 + eta), -(1 + eta)],
                               [-(1 - xi), -(1 + xi), (1 + xi), (1 - xi)]])

    def J_mat(self, xi, eta):
        """
        A method to return the jacobian matrix of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The jacobian matrix at position (xi, eta)
        """
        return np.matmul(self.GN(xi, eta), np.array([self.x, self.y]).T)

    def J(self, xi, eta):
        """
        A method to return the jacobian of the element at some isoparametric position (xi, eta)
        :param xi: The xi value of the isoparametric position
        :param eta: The eta value of the isoparametric position
        :return: The jacobian at position (xi, eta)
        """
        return np.linalg.det(self.J_mat(xi, eta))

    def quad_selector(self, n_gp):
        """
        Selects the necessary parameters for a desired Gauss quadrature
        :param n_gp: The desired number of Gauss points
        :return: The number of gauss points to be used (it will be changed if an invalid number of gauss points was
        chosen), the xi positions of the gauss points, the weights of each gauss point
        """
        # A bunch of if-else statements to determine the values required for the inputted number of gauss points
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        else:
            # If the inputted number of gauss points is not valid then a 5th order gauss quad is used for accuracy
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        return n_gp, xi, weights

    def iso_to_physical(self, xi, eta):
        """
        Determines the (x,y) position given a (xi, eta) position in the element
        :param xi: The value of the xi position
        :param eta: The value of the eta position
        :return: The (x,y) position in a 1x2 numpy array
        """
        # The numpy.dot function is misleading. It actually acts more like matrix multiplication than a dot product
        return np.dot(self.N(xi, eta), self.pos)

    def N_edge(self, edge, xi):
        """
        Determines the shape function matrix along a given edge
        :param edge: The index of the edge
        :param xi: The xi (or eta) value along the edge
        :return: The shape function matrix at the given xi (or eta) value on the given edge
        """
        if edge == 0:
            return self.N(xi, -1)
        elif edge == 1:
            return self.N(1, xi)
        elif edge == 2:
            return self.N(xi, 1)
        elif edge == 3:
            return self.N(-1, xi)

    def gauss(self, n_gp, integrand):
        """
        Uses gauss quadrature to integrate a function over an element
        :param n_gp: The number of gauss points desired
        :param integrand: The function to be integrated (in terms of xi and eta)
        :return: The value of the integrated function (could be a scalar/vector value, it will return the same form of
        the function that it is given)
        """
        n_gp, xi, weights = self.quad_selector(n_gp)    # Getting the gauss points, positions and weights
        eta = xi
        integrated = 0
        # Applying gauss quadrature
        for i in range(n_gp):
            for j in range(n_gp):
                integrated += self.J(xi[i], eta[j]) * weights[i] * weights[j] * integrand(xi[i], eta[j])
        return integrated


class P1:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
        self.n_nodes = 3
        self.edges = 3
        self.x = x_nodes
        self.y = y_nodes
        self.g_nodes = g_node_numbers
        DOF = 2 * np.array([g_node_numbers[0],
                            g_node_numbers[0]+0.5,
                            g_node_numbers[1],
                            g_node_numbers[1] + 0.5,
                            g_node_numbers[2],
                            g_node_numbers[2] + 0.5,
                            ])
        self.g_DOF = DOF.astype(int)
        self.el_num = el_num
        self.M = np.array([[1, 1, 1],
                           x_nodes,
                           y_nodes]).T
        self.A = 0.5 * np.linalg.det(self.M)
        self.B_mat = (1/(2 * self.A)) * np.array([[y_nodes[1] - y_nodes[2], 0, y_nodes[2] - y_nodes[0], 0, y_nodes[0] - y_nodes[1], 0],
                                                  [0, x_nodes[2] - x_nodes[1], 0, x_nodes[0] - x_nodes[2], 0, x_nodes[1] - x_nodes[0]],
                                                  [x_nodes[2] - x_nodes[1], y_nodes[1] - y_nodes[2], x_nodes[0] - x_nodes[2], y_nodes[2] - y_nodes[0], x_nodes[1] - x_nodes[0], y_nodes[0] - y_nodes[1]]])
        self.el_K = np.zeros([6, 6])
        self.el_F = np.zeros(6).T
        self.el_F_bound = np.zeros(6).T
        self.el_F_bod = np.zeros(6).T
        self.d = np.zeros(6)
        self.pos = np.array([x_nodes[0], y_nodes[0], x_nodes[1], y_nodes[1], x_nodes[2], y_nodes[2]])

    def B(self, xi, eta):
        return self.B_mat

    def N(self, xi, eta):
        return np.array([[xi, 0, eta, 0, 1 - xi - eta, 0],
                         [0, xi, 0, eta, 0, 1 - xi - eta]])

    # def Nxy(self, x, y):
    #     P = np.array([1, x, y])
    #     return np.matmul(P, np.linalg.inv(self.M))

    def in_element(self, x, y):
        P = np.array([1, x, y])
        xi = np.matmul(P, np.linalg.inv(self.M))
        return np.sum(np.add(1 < xi, xi < 0)) == 0

    def display(self):
        print("Element number = ", self.el_num + 1, "\nx = ", self.x, "\ny = ", self.y, "\nGlobal nodes = ", self.g_nodes + 1)

    def quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([1 / 3])
            eta = np.array([1 / 3])
            weights = np.array([1])
        elif n_gp == 2:
            print("Cannot have 2 Gauss points. Three gauss points have been used instead")
            n_gp = 3
            xi = np.array([1 / 6, 2 / 3, 1 / 6])
            eta = np.array([1 / 6, 1 / 6, 2 / 3])
            weights = np.array([1 / 3, 1 / 3, 1 / 3])
        elif n_gp == 3:
            xi = np.array([1 / 6, 2 / 3, 1 / 6])
            eta = np.array([1 / 6, 1 / 6, 2 / 3])
            weights = np.array([1 / 3, 1 / 3, 1 / 3])
        elif n_gp == 4:
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        elif n_gp > 4:
            print("Max gauss points available is 4. Hence 4 gauss points used instead.")
            n_gp = 4
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        else:
            print("Invalid value chosen for number of Gauss points. 4 Gauss points used instead for accuracy")
            n_gp = 4
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        return n_gp, xi, eta, weights

    def iso_to_physical(self, xi, eta):
        return np.dot(self.N(xi, eta), self.pos)


    def N_edge(self, edge, xi):
        xi = (1+xi)/2
        if edge == 0:
            return self.N(xi, 1 - xi)
        elif edge == 1:
            return self.N(0, xi)
        elif edge == 2:
            return self.N(xi, 0)

    def gauss(self, n_gp, integrand):
        n_gp, xi, eta, weights = self.quad_selector(n_gp)
        integrated = 0
        for i in range(n_gp):
            integrated += self.A * weights[i] * integrand(xi[i], eta[i])
        return integrated


class Q2:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
        self.n_nodes = 9
        self.edges = 4
        self.x = x_nodes
        self.y = y_nodes
        self.g_nodes = g_node_numbers
        # self.pos = np.array([x_nodes[0], y_nodes[0], x_nodes[1], y_nodes[1], x_nodes[2], y_nodes[2], x_nodes[3], y_nodes[3]])
        self.pos = np.zeros(18)
        DOF = np.zeros(18)
        for i in range(9):
            DOF[2*i] = int(2 * g_node_numbers[i])
            DOF[2*i + 1] = int(2 * g_node_numbers[i] + 1)
            self.pos[2*i] = x_nodes[i]
            self.pos[2*i + 1] = y_nodes[i]
        self.g_DOF = DOF.astype(int)
        self.el_num = el_num
        self.el_K = np.zeros([18, 18])
        self.el_F = np.zeros(18).T
        self.el_F_bound = np.zeros(18).T
        self.el_F_bod = np.zeros(18).T
        self.d = np.zeros(18)

    def N_single(self, xi):
        return np.array([[0.5*(xi-1)*xi],
                         [(1-xi)*(1+xi)],
                         [0.5*xi*(1+xi)]])

    def N(self, xi, eta):
        i = np.outer(self.N_single(eta), self.N_single(xi))
        N_row = np.array([i[0, 0],
                          i[0, 2],
                          i[2, 2],
                          i[2, 0],
                          i[0, 1],
                          i[1, 2],
                          i[2, 1],
                          i[1, 0],
                          i[1, 1]])
        return np.array([np.insert(np.zeros(9), np.arange(9), N_row),
                         np.insert(np.zeros(9), np.arange(9)+1, N_row)])


    def dN_single(self, xi):
        return np.array([[0.5*(2*xi-1)],
                         [-2*xi],
                         [0.5*(1+2*xi)]])

    def GN(self, xi, eta):
        out = np.zeros([2, 9])

        dN_dXi = np.outer(self.N_single(eta), self.dN_single(xi))
        out[0, :] = np.array([dN_dXi[0, 0],
                              dN_dXi[0, 2],
                              dN_dXi[2, 2],
                              dN_dXi[2, 0],
                              dN_dXi[0, 1],
                              dN_dXi[1, 2],
                              dN_dXi[2, 1],
                              dN_dXi[1, 0],
                              dN_dXi[1, 1]])

        dN_dEta = np.outer(self.dN_single(eta), self.N_single(xi))
        out[1, :] = np.array([dN_dEta[0, 0],
                              dN_dEta[0, 2],
                              dN_dEta[2, 2],
                              dN_dEta[2, 0],
                              dN_dEta[0, 1],
                              dN_dEta[1, 2],
                              dN_dEta[2, 1],
                              dN_dEta[1, 0],
                              dN_dEta[1, 1]])
        return out


    def B(self, xi, eta):
        B_scalar = np.matmul(np.linalg.inv(self.J_mat(xi, eta)), self.GN(xi, eta))
        return np.array([np.insert(np.zeros(9), np.arange(9), B_scalar[0, :]),
                         np.insert(np.zeros(9), np.arange(9)+1, B_scalar[1, :]),
                         np.insert(B_scalar[0, :], np.arange(9), B_scalar[1, :])])

    def J_mat(self, xi, eta):
        return np.matmul(self.GN(xi, eta), np.array([self.x, self.y]).T)

    def J(self, xi, eta):
        return np.linalg.det(self.J_mat(xi, eta))


    def quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        elif n_gp == 5:
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
            n_gp = 5
            xi = np.array([-(1/3)*(5-2*((10/17)**0.5))**0.5,
                           (1/3)*(5-2*((10/17)**0.5))**0.5,
                           -(1/3)*(5+2*((10/17)**0.5))**0.5,
                           (1/3)*(5+2*((10/17)**0.5))**0.5,
                           0])

            weights = np.array([(322 + 13*(70**0.5))/900,
                                (322 + 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                (322 - 13*(70**0.5))/900,
                                128/225])
        return n_gp, xi, weights

    def iso_to_physical(self, xi, eta):
        return np.dot(self.N(xi, eta), self.pos)

    def N_edge(self, edge, xi):
        if edge == 0:
            return self.N(xi, -1)
        elif edge == 1:
            return self.N(1, xi)
        elif edge == 2:
            return self.N(xi, 1)
        elif edge == 3:
            return self.N(-1, xi)

    def gauss(self, n_gp, integrand):
        n_gp, xi, weights = self.quad_selector(n_gp)
        eta = xi
        integrated = 0
        for i in range(n_gp):
            for j in range(n_gp):
                integrated += self.J(xi[i], eta[j]) * weights[i] * weights[j] * integrand(xi[i], eta[j])
        return integrated

