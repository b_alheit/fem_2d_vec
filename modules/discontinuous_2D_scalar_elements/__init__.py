import numpy as np


class P0:
    def __init__(self, node_numbers):
        self.nodes = 1
        self.node_numbers = node_numbers
        self.p = np.empty(1, dtype=float)

    def N_tilde(self, xi, eta):
        return np.array([1])


class P1:
    def __init__(self, node_numbers):
        self.nodes = 3
        self.node_numbers = node_numbers
        self.p = np.empty(3, dtype=float)

    def N_tilde(self, xi, eta):
        return np.array([1-xi-eta, xi, eta])
