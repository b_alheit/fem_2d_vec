try:
    import sys
    sys.path.append(sys.path[0][:sys.path[0].find('fem_2d_vec')] + 'fem_2d_vec/modules')
    import Mesh_2D_Vec
    from continuous_2D_vector_elements import P1 as lt
except:
    print("Modules may be imported incorrectly due to invalid directory structure.\n"
          "Structure should be: ./fem_2d_vec/.../<this script>.py\n"
          "                     ./fem_2d_vec/modules")
import numpy as np

x_wall = 19


def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1000


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return 0.2

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 2.1e10


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    return np.array([0, 0])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    if abs(x - x_wall) < 0.000001:
        return np.array([1000*9.80665*thick(x,y)*(300 - y), 0])
    else:
        return np.array([0, 0])



def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if y == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


nodes_x = np.array([x_wall, 100, 200, 300, x_wall, 100, 200, x_wall, 100, x_wall, 100])    # The x positions of the nodes
nodes_y = np.array([0, 0, 0, 0, 100, 100, 100, 200, 200, 300, 300])    # The y positions of the nodes

BC = np.zeros(len(nodes_x)*2)
BC[0:8] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

ICA = np.array([[1, 6, 5],
                [1, 2, 6],
                [2, 7, 6],
                [2, 3, 7],
                [3, 4, 7],
                [5, 9, 8],
                [5, 6, 9],
                [6, 7, 9],
                [8, 11, 10],
                [8, 9, 11]])  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)


# Creating a triangle mesh object with given set up
tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x, nodes_y, ICA, E, v, t_bar, u_bar, b, BC, "strain", thick, lt)
# tri_mesh.display_mesh("af")
# tri_mesh.display_mesh("big mesh", False, False, False)
tri_mesh.int_F_bound(3)
tri_mesh.int_K(1)   # Performing integration to create stiffness matricies
tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
tri_mesh.assemble()     # Assembling element matrices and vectors into global
tri_mesh.solve_d()      # Solving for global d

print("Deflection: ", tri_mesh.d)    # Displaying node deflections
print("Node 10 deflection in mm: ", 1000*tri_mesh.d[18:20])    # Displaying node deflections
# print("Original node positions: ", tri_mesh.pos)    # Displaying old node positions
# print("New node positions: ", tri_mesh.pos + tri_mesh.d)    # Displaying new node positions

e1 = tri_mesh.Elements[0]   # Creating a reference to element 1
# e2 = tri_mesh.Elements[1]   # Creating a reference to element 2
e1_stress = np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e1.B(0, 0), e1.d))
print("Stress in el 1: ", e1_stress) # Calculating and displaying stress in element 1
# print("Stress in el 2: ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e2.B(0, 0), e2.d))) # Calculating and displaying stress in element 2
print("Von Mises stress in el 1:", (e1_stress[0] ** 2 + e1_stress[1] ** 2 + 2*(e1_stress[2] ** 2)) ** 0.5)
tri_mesh.plot_displacement("Plane strain beam solution", 50, 1, False, False, False)   # Plotting the old and new node positions
