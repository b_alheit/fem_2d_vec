try:
    import sys
    sys.path.append(sys.path[0][:sys.path[0].find('fem_2d_vec')] + 'fem_2d_vec/modules')
    import Mesh_2D_Vec
    import continuous_2D_vector_elements
except:
    print("Modules may be imported incorrectly due to invalid directory structure.\n"
          "Structure should be: ./fem_2d_vec/.../<this script>.py\n"
          "                     ./fem_2d_vec/modules")

import numpy as np
import math

def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 1000


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return 0.343

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 1.1e5


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    return np.array([0, 0])


def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if math.fabs(x) < 0.0000001 or math.fabs(y) < 0.00000001:
        return np.array([0, 0])
    else:
        return np.array([None, None])


theta = math.pi/10
nodes_x = np.array([5, 7,
                    5*math.cos(theta), 7*math.cos(theta),
                    5*math.cos(2*theta), 7*math.cos(2*theta),
                    5*math.cos(3*theta), 7*math.cos(3*theta),
                    5*math.cos(4*theta), 7*math.cos(4*theta),
                    0, 0])    # Calculating the x positions of the nodes
nodes_y = np.array([0, 0,
                    5*math.sin(theta), 7*math.sin(theta),
                    5*math.sin(2*theta), 7*math.sin(2*theta),
                    5*math.sin(3*theta), 7*math.sin(3*theta),
                    5*math.sin(4*theta), 7*math.sin(4*theta),
                    5, 7])  # Calculating the y positions of the nodes

print("Node x positions:", nodes_x, '\n')
print("Node y positions:", nodes_y, '\n')

t_mag = 1.0e3 * thick(0, 0)     # Calculating the magnitude of t_bar for each element N/mm

def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    if math.sqrt(x**2 + y**2) < 5.1:
        if nodes_x[10] < x < nodes_x[8]:
            theta_t = 4.5 * theta
        elif nodes_x[8] < x < nodes_x[6]:
            theta_t = 3.5 * theta
        elif nodes_x[6] < x < nodes_x[4]:
            theta_t = 2.5 * theta
        elif nodes_x[4] < x < nodes_x[2]:
            theta_t = 1.5 * theta
        elif nodes_x[2] < x < nodes_x[0]:
            theta_t = 0.5 * theta
        else:
            print("There is something wrong with t_bar")
        return t_mag * np.array([math.cos(theta_t), math.sin(theta_t)])
    else:
        return np.array([0, 0])

BC = np.zeros(len(nodes_x)*2)
BC[[1, 3, 20, 22]] = 1

ICA = np.array([[1, 2, 4],
                [1, 4, 3],
                [3, 4, 6],
                [3, 6, 5],
                [5, 6, 8],
                [5, 8, 7],
                [7, 8, 10],
                [7, 10, 9],
                [9, 10, 12],
                [9, 12, 11],
                ])  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)

# Creating a triangle mesh object with given set up
tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec_Std(nodes_x, nodes_y, ICA, E, v, t_bar, u_bar, b, BC, "strain", thick, continuous_2D_vector_elements.P1)

tri_mesh.int_F_bound(3) # Performing integration to create boundary force vectors
tri_mesh.int_K(3)       # Performing integration to create stiffness matricies
tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
tri_mesh.assemble()     # Assembling element matrices and vectors into global
tri_mesh.solve_d()      # Solving for global d

print("Deflection : ", tri_mesh.d, "mm", '\n')    # Displaying node deflections
print("Node 8 deflection in mm: ", tri_mesh.d[[14, 15]], "mm", '\n')    # Displaying node 8 deflection

eA = tri_mesh.Elements[5]   # Creating a reference to element A
eB = tri_mesh.Elements[9]   # Creating a reference to element B

eA_stress = np.matmul(tri_mesh.E_mat(0, 0), np.matmul(eA.B(0, 0), eA.d))
eB_stress = np.matmul(tri_mesh.E_mat(0, 0), np.matmul(eB.B(0, 0), eB.d))

print("Stress in element A: ", eA_stress, "N/mm^2") # Displaying stress in element A
print("Stress in element B: ", eB_stress, "N/mm^2", '\n') # Displaying stress in element B

print("Von misses stress in element A: ", math.sqrt(eA_stress[0]**2 + eA_stress[1]**2 + 2*(eA_stress[2]**2)), "N/mm^2")  #Calculating and displaying mises stress in element A
print("Von misses stress in element B: ", math.sqrt(eB_stress[0]**2 + eB_stress[1]**2 + 2*(eB_stress[2]**2)), "N/mm^2", '\n')  #Calculating and displaying mises stress in element B

for i in range(len(tri_mesh.Elements)):
    e = tri_mesh.Elements[i]
    stress = np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e.B(0, 0), e.d))
    print("Von misses stress in element "+str(1+i)+": ", math.sqrt(stress[0]**2 + stress[1]**2 + 2*(stress[2]**2)), "N/mm^2")

print()
print("Magnitude of displacement should be the same for all internal and external nodes")
for i in range(int(len(tri_mesh.d)/2)):
    print("Magnitude of displacement for node "+str(1+i)+": ", np.linalg.norm(tri_mesh.d[[i*2, i*2+1]]))

tri_mesh.plot_displacement("Pipe solution",
                           scale=1,
                           lines_per_edge=1,
                           show_nodes=False,
                           show_nodes_numbers=False,
                           show_element_numbers=False)   # Plotting the old and new node positions